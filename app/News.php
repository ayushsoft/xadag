<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
     protected $table = "news";

     protected $fillable = [
	        'title',
	        'description',
	        'image',
	    ];


	 public function _select() {

        $select = [
            'news.id',
            'title',
            'description',
            'image',
        ];

        return $this->select($select);

    }

}
