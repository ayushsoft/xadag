<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
     protected $table = "products";

     protected $fillable = [
	        'name',
	        'price',
	        'save_time',
	        'savalgaa',
	        'zaavar',
	        'featured',
	        'product_type_id',
	        'description',
	        'info',
	        'orts',
	        'horigloh',
	        'image',
	        'special',
	    ];

	public function producttype() {
        return $this->belongsTo('App\Product_type', 'product_type_id');
    }
}
