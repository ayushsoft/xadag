<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cartproduct extends Model
{
    //
    protected $table = 'product_cart';

    protected $fillable = [
        'user_id', 'product_id', 'qty', 'total',
    ];

    public function Products() {
        return $this->belongsTo('App\Product', 'product_id');
    }

    /**
     * A Cart belongs to a User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function User() {
        return $this->belongsTo('App\User', 'user_id');
    }

}
