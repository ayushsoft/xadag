<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImg;
use App\Product_type;
use Illuminate\Support\Facades\File;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at','desc')->paginate(12);
        return view('backend.product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       
            $producttypes = Product_type::all();
            $selecttype= ['---'];
            foreach($producttypes as $producttype){
            $selecttype[$producttype->id] = $producttype->name;
        }


      







         return view('backend.product.create',compact('selecttype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


  


        if ($request->hasFile('image')){

            $image = $request->file('image');
            $imagename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(350,450)->save(public_path('uploads/product/' . $imagename  ));

            $product = new Product;
            $product->name = $request->name;
            $product->price = $request->price;
            $product->save_time = $request->save_time;
            $product->savalgaa = $request->savalgaa;
            $product->zaavar = $request->zaavar;
            $product->featured = $request->featured;
            $product->product_type_id = $request->product_type_id;
            $product->description     = $request->description;
            $product->info = $request->info;
            $product->orts = $request->orts;
            $product->horigloh = $request->horigloh;
            $product->image = $imagename;
            $product->save();

                        if ($request->hasFile('product_imgs'))
                        {

                            foreach ($request->product_imgs as $product_img) 
                                {
                                    $filename = $product_img->store('product_imgs');
                                    ProductImg::create([
                                        'product_id' => $product->id,
                                        'filename' => $filename
                                    ]);
                                }
        
                        }
  
        }

        else{
            $product = new Product;
            $product->create($request->all());
        }
      
        \Session::flash('flash_message','Бүтээгдэхүүн амжилттай нэмэгдлээ.');
        return redirect('product');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $productimgs = ProductImg::where('product_id','=',$product->id)->get();
        return view('backend.product.show',compact('product','productimgs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        

        $product = Product::find($id);
         $producttypes = Product_type::all();
            $selecttype= ['---'];
            foreach($producttypes as $producttype){
            $selecttype[$producttype->id] = $producttype->name;
        }
        $productimgs = ProductImg::where('product_id','=',$product->id)->get();
        return view('backend.product.edit',compact('product','selecttype','productimgs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        if ($request->hasFile('image')){

            $image = $request->file('image');
            $imagename = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(260,230)->save(public_path('uploads/product/' . $imagename  ));
            $product = Product::findOrFail($id);

             $product->update([
                 'name' => $request->input('name'),
                 'price' => $request->input('price'),
                 'save_time' => $request->input('save_time'),
                 'savalgaa' => $request->input('savalgaa'),
                 'zaavar' => $request->input('zaavar'),
                 'featured' => $request->input('featured'),
                 'product_type_id' => $request->input('product_type_id'),
                 'description' => $request->input('description'),
                 'info' => $request->input('info'),
                 'orts' => $request->input('orts'),
                 'horigloh' => $request->input('horigloh'),
                 'image' => $imagename,
        ]);
  
        }

        else{
            $product = Product::findOrFail($id);
            $product->update($request->all());
        }
      
        \Session::flash('flash_message','Бүтээгдэхүүн амжилттай нэмэгдлээ.');
        return redirect('product');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $product = Product::find($id);
         $product->delete();
        return redirect('product');
    }
    public function enable($id){
        $product =Product::find($id);
        $product->update([
            'special'=>1]);
         \Session::flash('flash_message','Бүтээгдэхүүн онцлох бүтээгдэхүүн боллоо.');
        return redirect('product');
    }
    public function disable($id){
        $product =Product::find($id);
        $product->update([
            'special'=>0]);
         \Session::flash('flash_message','Бүтээгдэхүүн онцлохоос хасагдлаа.');
        return redirect('product');
    }
}
