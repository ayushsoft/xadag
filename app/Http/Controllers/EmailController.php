<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Product;
use App\Product_type;

class EmailController extends Controller
{

    public function send($id){
        $producttypes = Product_type::all();
        $product = Product::find($id);
        //dd($product);
        return view('frontend.partials.email')->with('product',$product)->with('producttypes',$producttypes);
    }

    public function post(Request $request){

        $data = [
            'email' => $request->emailshare,
        ];

        Mail::send('frontend.partials.email',$data, function ($message) use ($data){
            $message->from('tsendayush0412@gmail.com','xadag.com');
            $message->to($data['email']);
            $message->subject('shine medee medeelel');
        });

        Session()->flash('email-share','Амжилттай илгээлээ. Баярлалаа');
        return redirect()->back();

    }
}
