<?php

namespace App\Http\Controllers;

use App\Cartproduct;
use Illuminate\Http\Request;
use App\Product_type;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Product;
use App\Cart;
use App\User;
class UserController extends Controller
{
    //

    public function index(){
        $producttypes = Product_type::all();
        $user_id = Auth::user()->id;
        $cart_products = Cartproduct::with('products')->where('user_id', '=', $user_id)->get();
        $count = Cartproduct::where('user_id', '=', $user_id)->count();
        $cart_total = Cartproduct::where('user_id', '=', $user_id)->sum('total');
        //dump($cart_total);
        return view('frontend.partials.cart',compact('producttypes','count'))
            ->with('cart_products',$cart_products)
            ->with('cart_total',$cart_total);
    }

    public function checkout(){
        $producttypes = Product_type::all();
        $user_id = Auth::user()->id;
        $cart_products = Cartproduct::with('products')->where('user_id', '=', $user_id)->get();
        $count = Cartproduct::where('user_id', '=', $user_id)->count();
        $cart_total = Cartproduct::where('user_id', '=', $user_id)->sum('total');
        //dump($cart_total);
        return view('frontend.partials.checkout',compact('producttypes','count'))
            ->with('cart_products',$cart_products)
            ->with('cart_total',$cart_total);
    }

    public function cardAdd(){
        $user_id = Auth::user()->id;

        $product_id = Input::get('product');
        $qty = Input::get('qty');

        $product = Product::find($product_id);



        Cartproduct::create(
            array (
                'user_id'    => $user_id,
                'product_id' => $product_id,
                'qty'        => $qty,
                'total'      => $product->price,
            )
        );
        Session()->flash('success','Таны сонгосон бүтээгдэхүүн амжилттай нэмэгдлээ, Tа миний сагс цэсэнд дарж орон худалдан авалт хийнэ үү. Баярлалаа');

        return redirect()->back();
    }

    public function  show(){
        return view('user.home');
    }
    public function profile(){
        $user = User::all();
        return view('user.partials.profile',compact('user'));
    }
}
