<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductImg;
use App\Product;

class ProductImgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $product= Product::find($request->product_id);

        if ($request->hasFile('product_imgs'))
                        {

                            foreach ($request->product_imgs as $product_img) 
                                {
                                    $filename = $product_img->store('product_imgs');
                                    ProductImg::create([
                                        'product_id' => $product->id,
                                        'filename' => $filename
                                    ]);
                                }
        
                        }
         return \App::make('redirect')->back()->with('flash_success', 'Thank you,!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_img =ProductImg::find($id);
        $product_img->delete();
         return \App::make('redirect')->back()->with('flash_success', 'Thank you,!');
    }
}
