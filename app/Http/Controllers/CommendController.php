<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Commend;
class CommendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commends =Commend::all();
        return view('backend.commend.index',compact('commends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
             return \App::make('redirect')->back()->with('flash_success', 'Thank you,!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $commend = new Commend;
        $commend->name = $request->name;
        $commend->user_id = $request->user_id;
        $commend->product_id = $request->product_id;
        $commend->save();
           return \App::make('redirect')->back()->with('flash_success', 'Thank you,!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $commend = Commend::find($id);
        return view('backend.commend.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commend = Commend::find($id);
        $commend->delete();
          return \App::make('redirect')->back()->with('flash_success', 'Thank you,!');
    }
}
