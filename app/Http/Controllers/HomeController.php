<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Product;
use App\Product_type;
use App\ProductImg;
use App\User;
use App\Commend;
use Illuminate\Support\Facades\Input;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    public function frontNews(){
        $news = News::all();
        $user = User::all();
        $product = Product::all();
          $producttypes = Product_type::all();
            $selecttype= ['---'];
            foreach($producttypes as $producttype){
            $selecttype[$producttype->id] = $producttype->name;
        }

        $last = Product::orderBy('created_at','desc')->take(10)->get();

        return view('frontend.pages.home',compact('news','product','selecttype','producttypes','last','user'));
    }
    
    public function productDesc($id,$code=null){
        $producttypes = Product_type::all();
        $product = Product::find($id);
        $productimgs = ProductImg::where('product_id','=',$product->id)->get();
          $commends = Commend::where('product_id','=',$id)->paginate(10);

        //dump($code);
        return view('frontend.pages.product-list',compact('product','productimgs','commends','producttypes','code'))
            ->with('product',$product);
    }

    public function productGrid(){
        $producttypes = Product_type::all();
        $product = Product::all();
        return view('frontend.pages.product-grid',compact('product','producttypes'));
    }

    public function productCat($id){
        $product = Product::where('product_type_id',$id)->get();
        $producttype =Product_type::find($id);
        $producttypes = Product_type::all();
        return view('frontend.pages.product-cat',compact('producttypes','product','producttype'));
    }

    public function createUser(Request $request)
    {
        User::create([
            'name' => 'name',
            'email' => $request->email,
            'type'=> 'user',
            'password' => bcrypt($request->password),
        ]);
        return redirect('/loginpage');
    }

    public function search(){
        $producttypes = Product_type::all();
        $product = Product::all();
        $query = Input::get('search');

        $search = Product::where('name', 'LIKE', '%' . $query . '%')->paginate(12);

        return view('frontend.partials.search',compact('product','producttypes','search','query'));
    }

    public function bonus(){
        $producttypes = Product_type::all();
        $product = Product::all();
        return view('frontend.partials.bonus',compact('producttypes','product'));
    }

}
