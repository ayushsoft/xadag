<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Product;
use App\Product_type;
use App\ProductImg;
use Illuminate\Support\Facades\File;
use Image;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view('backend.news.index',compact('news'));
    }
    public function pageshowall()
    {
        $news = News::all();
        $product = Product::all();
        $producttypes = Product_type::all();

        return view('frontend.pages.news',compact('news','producttypes','product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         if ($request->hasFile('image')){

            $avatar = $request->file('image');
            $imagename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(260,230)->save(public_path('uploads/news/' . $imagename  ));

            $news = new News;
            $news->title = $request->title;
            $news->description = $request->description;
            $news->image = $imagename;
            $news->save();  
            return redirect('news');  

        }
        else{

                News::create($request->all());
                return redirect('news');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);

        return view('backend.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);

        return view('backend.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::find($id);

        if ($request->hasFile('image')){

            $avatar = $request->file('image');
            $imagename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(260,230)->save(public_path('uploads/news/' . $imagename  ));

            $news->title = $request->title;
            $news->description = $request->description;
            $news->image = $imagename;
            $news->save();  
            return redirect('news');  

        }
        else{

                $news->update($request->all());
                return redirect('news');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::find($id);
        $news->delete();
        return redirect('news');
    }
}
