<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('price');
            $table->string('save_time')->nullable();
            $table->string('savalgaa')->nullable();
            $table->string('zaavar')->nullable();
            $table->integer('featured')->nullable();
            $table->integer('product_type_id')->unsigned()->nullable();
            $table->foreign('product_type_id')->references('id')->on('product_type')->onDelete('set null');
            $table->longText('description')->nullable();
            $table->longText('info')->nullable();
            $table->longText('orts')->nullable();
            $table->longText('horigloh')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table){
            $table->dropForeign('products_product_type_id_foreign');
    });
        Schema::drop('products');
    }
}
