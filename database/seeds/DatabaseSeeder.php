<?php

use Illuminate\Database\Seeder;
use App\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
                 'name' => 'admin',
                 'email' => 'admin@yahoo.com',
                 'type'=>'Admin',
                 'password' => bcrypt('1234qwer'),
        ]);
         $this->command->info('User table seeded!');
    }
}
