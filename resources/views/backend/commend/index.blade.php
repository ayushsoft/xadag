@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')

                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Бүтээгдэхүүн оруулсан саналууд</h3>
                    </div>
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Бүтээгдэхүүн оруулсан саналууд</h4>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Бүтээгдэхүүн</th>
                                                    <th>Сэтгэгдэл оруулсан</th>
                                                    <th>Сэтгэгдэл</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($commends as $key=>$commend)
                                                <tr>
                                     
                                                    <td>{{$key}}</td>
                                                    <td>{{$commend->product->name}}</td>
                                                    <td>{{$commend->user->name}}</td>
                                                    <td>{{$commend->name}}</td>
                                                    <td><a href="{{url('/deletecommend',$commend->id)}}" class="btn btn-danger" >Устгах</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div><!-- Row -->
                </div>


            </div>
</div>
@stop