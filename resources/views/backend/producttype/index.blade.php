@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')

               <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Бүтээгдэхүүний төрөлийн жагсаалт </h3>
                    </div>
                <div id="main-wrapper">
       
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Бүтээгдэхүүний төрөлүүд</h4>
                                </div>
                                <div style="padding-bottom: 30px;">  <a class="btn btn-primary" href="{{route('producttype.create')}}">Төрөл нэмэх</a></div>
                                  
                              

                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>Нэр</th>
                                                    <th>Засвар</th>

                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($producttypes as $key=>$type)
                                                <tr>
                                                    <th scope="row">{{$key}}</th>
                                                    <td>{{$type->name}}</td>
                                                    <td>
                                                        <a href="{{route('producttype.edit',$type->id)}}" class="btn btn-warning">Засах</a>
                                                        <a href="{{url('/deleteproducttype',$type->id)}}" class="btn btn-danger">Устгах</a>
                                                    </td>


                                                @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>


            </div>


            </div>
</div>