@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')
                <!-- /Page Header -->
                <!-- Page Inner -->
                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Удирдах самбар</h3>
                    </div>
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-white stats-widget">
                                    <div class="panel-body">
                                        <div class="pull-left">
                                            <span class="stats-number">35</span>
                                            <p class="stats-info">Нийт бүтээгдэхүүн</p>
                                        </div>
                                        <div class="pull-right">
                                            <i class="icon-arrow_upward stats-icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-white stats-widget">
                                    <div class="panel-body">
                                        <div class="pull-left">
                                            <span class="stats-number">2</span>
                                            <p class="stats-info">Мэдээ</p>
                                        </div>
                                        <div class="pull-right">
                                            <i class="icon-arrow_upward stats-icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-white stats-widget">
                                    <div class="panel-body">
                                        <div class="pull-left">
                                            <span class="stats-number">6</span>
                                            <p class="stats-info">Нийт хэрэглэгч</p>
                                        </div>
                                        <div class="pull-right">
                                            <i class="icon-arrow_upward stats-icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="panel panel-white stats-widget">
                                    <div class="panel-body">
                                        <div class="pull-left">
                                            <span class="stats-number">1</span>
                                            <p class="stats-info">Худалдан авалт</p>
                                        </div>
                                        <div class="pull-right">
                                            <i class="icon-arrow_upward stats-icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                        
                        <div class="row">
                            <div class="col-lg-5 col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title">Projects</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="project-stats">
                                            <ul class="list-unstyled">
                                                <li>Alpha - Admin Template<span class="label label-default pull-right">85%</span></li>
                                                <li>Meteor - Landing Page<span class="label label-success pull-right">Finished</span></li>
                                                <li>Modern - Corporate Website<span class="label label-success pull-right">Finished</span></li>
                                                <li>Space - Admin Template<span class="label label-danger pull-right">Rejected</span></li>
                                                <li>Backend UI<span class="label label-default pull-right">27%</span></li>
                                                <li>Personal Blog<span class="label label-default pull-right">48%</span></li>
                                                <li>E-mail Templates<span class="label label-default pull-right">Pending</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-heading clearfix">
                                        <h4 class="panel-title">Сүүлийн худалдан авалт</h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsive invoice-table">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">1</th>
                                                        <td>Kenny Highland</td>
                                                        <td>Themeforest</td>
                                                        <td><span class="label label-success">Амжилттай</span></td>
                                                        <td>$427</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">2</th>
                                                        <td>Darrell Price</td>
                                                        <td>Twitter</td>
                                                        <td><span class="label label-success">Амжилттай</span></td>
                                                        <td>$1714</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">3</th>
                                                        <td>Richard Lunsford</td>
                                                        <td>Facebook</td>
                                                        <td><span class="label label-danger">амжилтгүй</span></td>
                                                        <td>$685</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">4</th>
                                                        <td>Amy Walker</td>
                                                        <td>Youtube</td>
                                                        <td><span class="label label-warning">Цуцлагдсан</span></td>
                                                        <td>$9900</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">5</th>
                                                        <td>Kathy Olson</td>
                                                        <td>Youtube</td>
                                                        <td><span class="label label-success">Амжилттай</span></td>
                                                        <td>$1250</td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">6</th>
                                                        <td>Susan Mabry</td>
                                                        <td>Instagram</td>
                                                        <td><span class="label label-success">Амжилттай</span></td>
                                                        <td>$399</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Row -->
                        
                    </div><!-- Main Wrapper -->
                    <div class="page-footer">
                        <p>Лаклайф </i>2018 он</p>
                    </div>
                </div><!-- /Page Inner -->
                
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
@stop