<div class="page-sidebar">
    <a class="logo-box" href="/">
        <span>Xadag.com</span>
        <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
        <i class="icon-close" id="sidebar-toggle-button-close"></i>
    </a>
    <div class="page-sidebar-inner">
        <div class="page-sidebar-menu">
            <ul class="accordion-menu">
                <li class="active-page">
                    <a href="{{url('/user/account/info')}}">
                        <i class="menu-icon icon-home4"></i><span>Хэрэглэгчийн хэсэг</span>
                    </a>
                </li>

                <li>
                    <a href="{{url('/user/product/show')}}">
                        <i class="fa fa-heart"></i><span style="margin-left: 10px;">Таны авсан бүтээгдэхүүн</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/user/cupon/show')}}">
                        <i class="fa fa-file-code-o"></i><span style="margin-left: 10px;">Урамшуулал</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-users"></i><span style="margin-left: 10px;">Хувийн мэдээлэл</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-newspaper-o"></i><span style="margin-left: 10px;">Тохиргоо</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div><!-- /Page Sidebar -->