 <div class="page-sidebar">
                <a class="logo-box" href="/">
                    <span>Лаклайф</span>
                    <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
                    <i class="icon-close" id="sidebar-toggle-button-close"></i>
                </a>
                <div class="page-sidebar-inner">
                    <div class="page-sidebar-menu">
                        <ul class="accordion-menu">
                            <li class="active-page">
                                <a href="{{url('/admin')}}">
                                    <i class="menu-icon icon-home4"></i><span>Удирдах самбар</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('news.index')}}">
                                    <i class="fa fa-newspaper-o"></i><span>  Мэдээлэл</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('product.index')}}">
                                    <i class="fa fa-heart"></i><span>  Бүтээгдэхүүн</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('producttype.index')}}">
                                    <i class="fa fa-file-code-o"></i><span>  Бүтээгдэхүүний төрөл</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-users"></i><span>  Үйлчлүүлэгчид</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-newspaper-o"></i><span>  Сэтгэгдэлүүд</span>
                                </a>
                            </li>
                           
                        </ul>
                    </div>
                </div>
            </div><!-- /Page Sidebar -->