<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>LuckLife</title>
       
        <!-- Fonts -->
        <link href="{{URL::asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/plugins/icomoon/style.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/plugins/uniform/css/default.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet"/>
        <link href="{{URL::asset('assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet">  
      
        <!-- Theme Styles -->
        <link href="{{URL::asset('assets/css/space.min.css')}}" rel="stylesheet">
        <link href="{{URL::asset('assets/css/custom.css')}}" rel="stylesheet">

        <!-- Styles -->
    </head>
    <body>

        @yield('content');


         <script src="{{URL::asset('assets/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/uniform/js/jquery.uniform.standalone.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/switchery/switchery.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/d3/d3.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/nvd3/nv.d3.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/flot/jquery.flot.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/flot/jquery.flot.time.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/flot/jquery.flot.symbol.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/flot/jquery.flot.resize.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/flot/jquery.flot.pie.min.js')}}"></script>
        <script src="{{URL::asset('assets/plugins/chartjs/chart.min.js')}}"></script>
        <script src="{{URL::asset('assets/js/space.min.js')}}"></script>
        <script src="{{URL::asset('assets/js/pages/dashboard.js')}}"></script>

    </body>


</html>
