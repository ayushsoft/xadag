@extends('backend.app')

@section('content')
    <?php function dazo_substr_small($txt='',$limit = 141){
        $buf = '';
        $k=0; //үсгийн байрлалыг тодорхойлоход ашиглагдана.
        for($i=0;$i<strlen($txt);$i++){

            $k++;
            $buf .= $txt{$i};
            if(ord($txt{$i})>207 && ord($txt{$i})<212){ //крилл үсэг үсэг байх тохиолдолд хийх үйлдэл
                $i++;
                $buf .= $txt{$i};
            }
            if($k>=$limit){ //авах үсгийн хязгаарт хүрмэгч зогсоно
                return $buf;
            }
        }
        return $buf;
    }

    ?>

<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')

             
  <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Бүтээгдэхүүний жагсаалт </h3>
                    </div>

                <div id="main-wrapper">
       
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Бүтээгдэхүүнүүд</h4>
                                </div>
                                <div style="padding-bottom: 30px;">  <a class="btn btn-primary" href="{{route('product.create')}}">Бүтээгдэхүүн нэмэх</a></div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>Нэр</th>
                                                    <th>Үнэ</th>
                                                    <th>Хадгалах хугцаа</th>
                                                    <th>Савалгаа</th>
                                                    <th>Заавар</th>
                                               
                                                    
                                             
                                                    <th>Тухай</th>
                                                    
                                                    <th>Хоргилох</th>
                                                    <th>Зураг</th>
                                                    <th>Онцлох</th>
                                                     <th>Засвар</th>

                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($products as $key=>$product)
                                                <tr>
                                                    <th scope="row">{{$key}}</th>
                                                    <td><a href="{{route('product.show',$product->id)}}">{{$product->name}}</a></td>
                                                    <td>{{$product->price}}</td>
                                                    <td>{{$product->save_time}}</td>
                                                    <td>{{$product->savalgaa}}</td>
                                                    <td>{{dazo_substr_small($product->zaavar,80)}}...</td>
                                                    
                                                    <td>{{dazo_substr_small($product->info,80)}}...</td>
                                                   
                                                    <td>{{dazo_substr_small($product->horigloh,80)}}...</td>


                                                    <td>  {{ Html::image('uploads/product/'.$product->image, 'alt', [ 'width' => 80, 'height' => 60   ]) }}</td>
                                                    <td> @if($product->special == 0)
                                                    <a href ="{{url('/enableproduct',$product->id)}}" class="btn btn-primary"> Онцлох</a>
                                                    @else
                                                    <a href ="{{url('/disableproduct',$product->id)}}" class="btn btn-danger"> Болиулах</a>
                                                    @endif
                                                    
                                                    </td>
                                                    <td>
                                                        <a href="{{route('product.edit',$product->id)}}" class="btn btn-warning">Засах</a>
                                                        <a href="{{url('/deleteproduct',$product->id)}}" class="btn btn-danger">Устгах</a>
                                                    </td>


                                                @endforeach
                                                </tr>
                                            </tbody>
                                            {{$products->links()}}
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>

            </div>
</div>
@endsection
