@extends('backend.app')

@section('content')


<style type="text/css">

.carimg {
  position: relative;
  margin-top: 50px;
  width: 300px;
  height: 200px;

}
.button {
  position: absolute;
  width: 60px;
  left:220px;
  bottom: 160px;
  text-align: center;
  opacity: 0;
  transition: opacity .35s ease;
}

.button a {
  width: 60px;
  padding: 12px 48px;
  text-align: center;
  color: red;
  border: solid 2px red;
  z-index: 1;
}

.carimg:hover .button {
  opacity: 1;
}
</style>

<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')

                 <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Бүтээгдэхүүн нэмэх</h4>
                                </div>
                                <div class="panel-body">
                                    <div role="tabpanel">
                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Үндсэн мэдээлэл</a></li>
                                                <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Зураг засах</a></li>
                                              
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="tab1">
                                                     <div class="panel-body">
                                	{{Form::model($product,['method'=>'PATCH','action'=>['ProductController@update',$product->id],'files' => true] ) }}

                                 
                                         <div class="form-group">
                                            {!! Form::label('name','Нэр',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('name',$product->name,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Үнэ',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('price',$product->price,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Хадгалах хугцаа',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('save_time',$product->save_time,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Савалгаа',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('savalgaa',$product->savalgaa,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Заавар',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('zaavar',$product->zaavar,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','featured',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::number('featured',$product->featured,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>


                                         <div class="form-group">
                                        
                        
                                             {!! Form::label('name','Төрөл',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                            {{ Form::select('product_type_id',$selecttype,$product->product_type_id,['class'=>'form-control']) }}
                                            </div>
                                          </div>
                                          
                                         <div class="form-group">
                                            {!! Form::label('name','Дэлгэрэнгүй',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('description',$product->description,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Тухай',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('info',$product->info,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Орц',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('orts',$product->orts,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Хориглох',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('horigloh',$product->horigloh,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>

                                         <div class="form-group">
                                            {!! Form::label('name','Үндсэн зураг',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {{ Html::image('uploads/product/'.$product->image, 'alt', [ 'width' => 100, 'height' => 60   ]) }}
                                               {!!Form::file('image',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>

                                          

                                          <div class="form-group">
                                            <div class="text-center">{{Form::submit('Засах',['class'=>'btn btn-primary'])}}</div>
                                         
                                     </div>


                                    {!! Form::close() !!}
                                </div>
                                                </div>
          <div role="tabpanel" class="tab-pane" id="tab2">


             @foreach($productimgs as $productimg)
             <div class="col-md-4">
                   <div class="carimg">
                        {{ Html::image('app/'.$productimg->filename, 'alt', [ 'width' => 300, 'height' => 200 ]) }}
                        
                        <form method="POST" action="{{ url('/productimgdelete', $productimg->id) }}" class="delete_form">
                          {{ csrf_field() }}
                         <input type="hidden" name="_method" value="DELETE">

                          <button id="delete-btn1" class="button">
                              <i class="fa fa-trash delete-white"></i>
                         </button>
                        </form>
                      </div>

                      
              </div>
            @endforeach
            <div class="col-md-12">
              {{ Form::open(['route' => 'productimg.store','files' => true]) }}
              <div class="form-group">
                                              
         
                                              <div class="col-sm-10">
                                              {{ Form::file('product_imgs[]', ['multiple' => 'multiple','class' => 'form-control']) }}
                                              </div>
                                          </div>
                          {{Form::hidden('product_id',$product->id)}}

                          {{ Form::submit('Зураг нэмэх', array('class' => 'btn btn-primary')) }}

           {{ Form::close() }}

 				</div>
             </div>
             

                                               
                                             
                                                
                                            </div>
                                    </div>
                                </div>

                           
                         	
                      

                               
                            </div>



            </div>
</div>