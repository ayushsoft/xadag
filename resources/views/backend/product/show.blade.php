@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')


                 <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Бүтээгдэхүүний тухай </h4>
                                </div>

                                <div class="panel-body">
                                	<div class="col-md-6">
                                		<h3>Мэдээлэлүүд</h3>
                                		<b>ДД</b> : {{$product->id}}</br></br>
                                    	<b>Нэр</b>: {{$product->name}}</br></br>
                                    	<b>Үнэ</b>: {{$product->price}}</br></br>
                                    	<b>Хадгалах хугцаа:</b> {{$product->save_time}}</br></br>
                                    	<b>Заавар:</b> {{$product->zaavar}}</br></br>
                                    	<b>Харуулах:</b> {{$product->featured}}</br></br>
                                    	<b>Төрөл:</b> {{$product->producttype->name}}</br></br>
                                    	<b>Дэлгэрэнгүй:</b> {{$product->description}}</br></br>
                                    	<b>Тухай:</b> {{$product->info}}</br></br>
                                    	<b>Орц:</b> {{$product->orts}}</br></br>
                                    	<b>Хориглох:</b> {{$product->horigloh}}</br>
                                	<a href="{{route('product.edit',$product->id)}}" class="btn btn-primary">Засах</a>
                                	</div>
                                	<div class="col-md-6">
                                		<h3>Зураг</h3>
                                		 {{ Html::image('uploads/product/'.$product->image, 'alt', [ 'width' => 140, 'height' => 120   ]) }}</br>
                                		 <h4>Бусад зураг</h4></br>
                                		   @foreach($productimgs as $productimg)
                                		   {{ Html::image('app/'.$productimg->filename, 'alt', [ 'width' => 100, 'height' => 100 ]) }}
                                		   @endforeach


                                	</div>
                                	

                                </div>
                </div>
               


            </div>
</div>