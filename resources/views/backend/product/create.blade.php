@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')

                <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Бүтээгдэхүүн нэмэх</h4>
                                </div>
                                <div class="panel-body">
                                
                                    {!! Form::open(['route' => 'product.store','files' => true,'class'=>'form-horizontal']) !!}
                                         <div class="form-group">
                                            {!! Form::label('name','Нэр',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('name','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Үнэ',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('price','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Хадгалах хугцаа',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('save_time','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Савалгаа',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('savalgaa','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Бүтээгдэхүүний Заавар',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('zaavar','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','featured',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::number('featured','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>


                                         <div class="form-group">
                                        
                        
                                             {!! Form::label('name','Бүтээгдэхүүний Төрөл',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                            {{ Form::select('product_type_id',$selecttype,null,['class'=>'form-control']) }}
                                            </div>
                                          </div>
                                          
                                         <div class="form-group">
                                            {!! Form::label('name','Дэлгэрэнгүй тайлбар',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('description','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Үйчилгээ',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('info','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Орц найрлага',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('orts','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Анхаарах зүйлс',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('horigloh','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Үндсэн зураг',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::file('image',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>

                                          <div class="form-group">
                                              
                                              {!! Form::label('name','Бусад зураг',['class'=>'col-sm-2 control-label'])!!}
                                              <div class="col-sm-10">
                                              {{ Form::file('product_imgs[]', ['multiple' => 'multiple','class' => 'form-control']) }}
                                              </div>
                                          </div>

                                          <div class="form-group">
                                            <div class="text-center">{{Form::submit('Нэмэх',['class'=>'btn btn-primary'])}}</div>
                                         
                                     </div>


                                    {!! Form::close() !!}
                                </div>
                            </div>




            </div>
</div>
@stop