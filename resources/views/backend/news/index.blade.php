@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')

                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Мэдээлэлийн жагсаалт </h3>
                    </div>
                <div id="main-wrapper">
       
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Мэдээлэлүүд</h4>
                                </div>
                                <div style="padding-bottom: 30px;">  <a class="btn btn-primary" href="{{route('news.create')}}">Мэдээлэл нэмэх</a></div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th>Image</th>
                                                    <th>Settings</th>

                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($news as $key=>$new)
                                                <tr>
                                                    <th scope="row">{{$key}}</th>
                                                    <td>{{$new->title}}</td>
                                                    <td>{{$new->description}}</td>
                                                    <td>  {{ Html::image('uploads/news/'.$new->image, 'alt', [ 'width' => 80, 'height' => 60   ]) }}</td>
                                                    <td>
                                                        <a href="{{route('news.edit',$new->id)}}" class="btn btn-warning">Edit</a>
                                                        <a href="{{url('/deletenews',$new->id)}}" class="btn btn-danger">Delete</a>
                                                    </td>


                                                @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>


            </div>
        </div>
</div>