@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')
                  {{Form::model($news,['method'=>'PATCH','action'=>['NewsController@update',$news->id],'files' => true] ) }}

                                         <div class="form-group">
                                            {!! Form::label('name','Title',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('title',$news->title,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Description',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('description',$news->description,['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="text-center"> {{ Html::image('uploads/news/'.$news->image, 'alt', [ 'width' => 200, 'height' => 200   ]) }}</div>
                                        
                                         <div class="form-group">
                                            {!! Form::label('name','Image',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::file('image',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            <div class="text-center">{{Form::submit('Insert',['class'=>'btn btn-primary'])}}</div>
                                         
                                     </div>


                                    {!! Form::close() !!}


            </div>
</div>