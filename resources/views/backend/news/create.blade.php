@extends('backend.app')

@section('content')


<div class="page-container">
            <!-- Page Sidebar -->
            @include('backend.partials.leftbar')
            <!-- /Page Sidebar -->
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Header -->
                @include('backend.partials.navbar')




<div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Мэдээлэл нэмэх</h4>
                                </div>
                                <div class="panel-body">
                                
                                    {!! Form::open(['route' => 'news.store','files' => true,'class'=>'form-horizontal']) !!}
                                         <div class="form-group">
                                            {!! Form::label('name','Title',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::text('title','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            {!! Form::label('name','Description',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::textarea('description','',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                         <div class="form-group">
                                            {!! Form::label('name','Image',['class'=>'col-sm-2 control-label'])!!}
                                            <div class="col-sm-10">
                                               {!!Form::file('image',['class'=>'form-control'])!!}
                                            </div>
                                            
                                         </div>
                                          <div class="form-group">
                                            <div class="text-center">{{Form::submit('Insert',['class'=>'btn btn-primary'])}}</div>
                                         
                                     </div>


                                    {!! Form::close() !!}
                                </div>
                            </div>


            </div>
</div>
@stop