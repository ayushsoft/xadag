<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Нэвтэрсэн</a>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Үндсэн цэс</li>
            <li>
                <a href="{{url('user/account')}}">
                    <i class="fa fa-home"></i> <span>Эхлэл</span>

                </a>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-share-square-o"></i> <span>Санал болгож<br>зуучилсны орлого</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-opencart"></i> <span>Өөрийн худалдан<br> авалтын оноо</span>

                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-shirtsinbulk"></i> <span>Бүтээгдэхүүн</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-blue">4</small>
                     </span>
                </a>
            </li>
            <li>
                <a href="{{url('user/profile')}}">
                    <i class="fa fa-wrench"></i> <span>Тохиргоо</span>
                    <span class="pull-right-container">

                     </span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>