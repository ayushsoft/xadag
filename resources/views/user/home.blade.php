@extends('user.master.master')
@section('content')
    @include('user.partials.header')
    @include('user.partials.siderbar')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Хэрэглэгчийн хэсэг

            </h1>

        </section>

        <!-- Main content -->
        <section class="content" style="min-height: auto">
            <!-- Small boxes (Stat box) -->
            <div class="row">


                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Нийт худалдан авалтын тоо</span>
                            <span class="info-box-number">760</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Найздаа санал болгосны урамшуулал</span>
                            <span class="info-box-number">2,000 ₮</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>

        </section>

        <section class="content-header">
            <h1>
                Худалдан авалтын урамшуулал
            </h1>
            <h4>
                Таны худалдан авалт бүрийг урамшуулах үүднээс компанийн зүгээс дараах сугалаат урамшуулалыг санал болгож байна.
            </h4>
        </section>
        <section class="content">



            <div class="row">

                <div class="col-md-12">
                    <div class="box box-success">
                        <h4 style="margin-left: 20px;" class="box-title">7-аас дээш худалдан авалт хийсэн бол</h4>
                        <div class="box-body">
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p1.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p2.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p3.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p4.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-warning">
                        <h4 style="margin-left: 20px;" class="box-title">4-6 удаа худалдан авалт хийсэн бол</h4>
                        <div class="box-body">
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p5.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p6.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p7.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p8.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">

                <div class="col-md-12">
                    <div class="box box-danger">
                        <h4 style="margin-left: 20px;" class="box-title">1-3 удаа худалдан авалт хийсэн бол</h4>
                        <div class="box-body">
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p9.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p10.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p11.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="card-img-top"
                                     src="{{asset('data/option1/p12.jpg')}}"
                                     alt="Card image cap">
                                <div class="card-body">
                                    <p class="card-text">This content is a little bit longer.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </section>
        <!-- /.content -->
    </div>

    @include('user.partials.footer')
@endsection