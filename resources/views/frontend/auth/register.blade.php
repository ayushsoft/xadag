@extends('frontend.app')
@section('content')


@include('frontend.partials.navbar')

<div class="container">
		<div class="row">
			<div class="block block-breadcrumbs">
				<ul>
					<li class="home">
						<a href="#"><i class="fa fa-home"></i></a>
						<span></span>
					</li>
					<li>Шинэ хэрэглэгч</li>
				</ul>
			</div>
			<div class="main-page">
				<h1 class="page-title">Шинээр бүртгүүлэх</h1>
				<div class="page-content">
		            <div class="row">						
		            		<div class="col-sm-3"></div>
								<div class="col-sm-6">
				            	<div class="box-border">
				            	    {{Form::open(['method'=>'post','url'=>'/registeruser'])}}
				            
				            		<h4>Шинэ Хэрэглэгч Болох</h4>
									<p>
										<label></label>
										<input type="text" name="name" placeholder="Таны нэр">
									</p>
			            			<p>
			            				<label></label>
			            				<input type="text" name="email" placeholder="И-мэйл хаягаа оруулна уу">
			            			</p>
			            			<p>
			            				<label></label>
			            				<input type="password" name ="password" placeholder="Нууц үг" id="password" required>
									</p>
                                	<p><label></label><input type="password" placeholder="Нууц үгээ давтаж оруулна уу" id="confirm_password" required></p>
			            				<div class="col-md-3"><input type="submit" class="button" value="Бүртгүүлэх"></div>
			            				
			            				<div class="col-md-9">
			            				    Бүртгэлтэй хэрэглэгч бол? <a href="/loginpage" class="btn btn-primary" >Нэвтрэх</a>
			            				</div>
			            				
			            			</p>
			            		
			            			{{Form::close()}}
				            	</div>
				            	</div>

				            <div class="col-sm-3"></div>
		            	</div>
		        </div>
			</div>
		</div>

	</div>

	@include('frontend.partials.footer')
	<script>
	    var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

	</script>
@stop