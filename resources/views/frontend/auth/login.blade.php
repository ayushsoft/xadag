@extends('frontend.app')
@section('content')

@include('frontend.partials.navbar')
<div class="container">
		<div class="row">
			<div class="block block-breadcrumbs">
				<ul>
					<li class="home">
						<a href="#"><i class="fa fa-home"></i></a>
						<span></span>
					</li>
					<li>Нэвтрэх</li>
				</ul>
			</div>
			<div class="main-page">
				<h1 class="page-title">Нэвтрэх</h1>
				<div class="page-content">
		            <div class="row">
		            	<div class="col-sm-3"></div>
		            	<div class="col-sm-6">
		            		<div class="box-border">
		            		     <form method="POST" action="{{ route('login') }}">
		            		          {{ csrf_field() }}

		            			<p>
		            				<label>И-мэйл хаяг</label>
		            				<input type="text" name="email">
		            			</p>
		            			 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
		            			<p>
		            				<label>нууц үг</label>
		            				<input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                
		            			</p>

		            			<p>
		            				<a href="#">Нууц үгээ мартсан?</a>
		            			</p>
		            			<p>
		            				<button class="button"><i class="fa fa-lock"></i> Нэвтрэх</button>
		            			</p>

		            			 </form>
		            			
		            		</div>
		            	</div>
		            	<div class="col-sm-3"></div>
		            </div>
		        </div>
			</div>
		</div>

	</div>
	@include('frontend.partials.footer')
@stop