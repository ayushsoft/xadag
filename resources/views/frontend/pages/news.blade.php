@extends('frontend.app')
@section('content')


@include('frontend.partials.navbar')

<div class="container">
		<div class="row">
			<div class="block block-breadcrumbs">
				<ul>
					<li class="home">
						<a href="#"><i class="fa fa-home"></i></a>
						<span></span>
					</li>
					<li><a href="#">Мэдээ мэдээлэл</a><span></span></li>

				</ul>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-4 col-md-3">
					<div class="block block-widget">
						<div class="block-head">
							<h5 class="widget-title">Бүгд</h5>
						</div>
						<div class="block-inner">
							<ul class="list-link">
								<li><a href="#">Нөхөн сэргээх</a></li>
								<li><a href="#">Цэвэрлээгээний</a></li>
								<li><a href="#">Дархлааны</a></li>

							</ul>
						</div>
					</div>
					
					<div class="block block-top-sellers" style="border: 0.6px solid #01953f;">
								<div class="block-head" style="background: #57e492;">
									<div class="block-title">
										<div class="block-icon">
											<img src="{{asset('data/top-seller-icon.png')}}" alt="store icon">
										</div>
										<div class="block-title-text text-sm">Мэд, Мэдээл</div>
										<div class="block-title-text text-lg"> Ашиг ол</div>
									</div>
								</div>
								<div class="block-inner">

									<div class="block-inner">
										<div class="product">
											<div class="product-name">
												<h5 style="line-height: 20px;">
													<span style="color: #01953f;font-weight: bold;">10%-ИЙН ХӨНГӨЛӨЛТ</span>-ийг санал болгоод, үнийн дүнгийн <span style="text-transform: uppercase;color: red;font-weight: bold;">
														10%-тай
													тэнцэх урамшуулал</span>-ыг аваарай.
												</h5>
											</div>
											<div class="product-name">
												<h5 style="line-height: 20px;">
													Таны санал болгосон хүн худалдан авалт хийснээр та урамшуулал авах эрх
													нээгдэнэ.
												</h5>
											</div>

											<div class="swiper-container">
												<div class="swiper-wrapper">
													@foreach($product as $p)
														<div class="swiper-slide">
															<div class="image">
																<a href="#"><img src="/uploads/product/{{$p->image}}" alt="p23.jpg"></a>
															</div>
															<div class="product-name" >
																<a style="color: #01953f;text-align: center!important;font-weight: bold;" href="{{url('products',$p->id)}}">{{$p->name}}</a>
															</div>
															<div class="price-box">
																<span class="product-price">{{$p->price}}</span>
																<span class="product-price-old">{{$p->price}}</span>
															</div>
															<div class="price-box">
																<span class="product-price" style="color: #000;font-weight: 400;">Санал болгогч хүний код:</span>
																<span class="product-price" style="color: red;margin-left: 35%;" >
																	@if(Auth::check())
																		{{Auth::user()->code}}
																	@endif
																</span>
															</div>

														</div>
													@endforeach
												</div>
												<!-- Add Arrows -->
												<div class="swiper-button-next"></div>
												<div class="swiper-button-prev"></div>
											</div>

											<div class="price-box">
												<span class="product-price"></span>
												
											</div>	
											<div class="product-name">
												<h5 style="line-height: 20px;">
													Энэхүү тасалбарыг үзүүлэгч нь бүтээгдэхүүний анхны үнийн<br>
													<b style="color: #0a6aa1">10%-ийн</b> хөнгөлөлт эдэлнэ.
												</h5>
											</div>
										</div>

									</div>

								</div>
							</div>
					
					<!-- ./block SPECIALS -->
				</div>

				<div class="col-xs-12 col-sm-8 col-md-9">
					<h1 class="page-title">Мэдээ мэдээлэл</h1>
					<div class="main-page">
						<div class="page-content clearfix">
							<ul class="blog-posts">
								@foreach($news as $new)
								<li class="post-item">
									<article class="entry">
										<div class="entry-ci">
											<div class="entry-thumb image-hover2">
												<a href="post.html">
													<img src="uploads/news/{{$new->image}}" alt="Blog">
												</a>
											</div>
											<h3 class="entry-title"><a href="{{url('news-desc',$new->id)}}">{!! $new->title !!}</a></h3>
											<div class="entry-meta-data">
	                                            <span class="author">
	                                            <i class="fa fa-user"></i>
	                                            by: <a href="#">Admin</a></span>
												<span class="cat">
	                                                <i class="fa fa-folder-o"></i>
	                                                <a href="#">News, </a>
	                                            </span>

												<span class="date"><i class="fa fa-calendar"></i> 2014-08-05 07:01:49</span>
											</div>

											<div class="entry-excerpt">
												{!! $new->description !!}
											</div>
											<div class="entry-more">
												<a class="button" href="{{url('news-desc',$new->id)}}">Дэлгэрэнгүй</a>
											</div>
										</div>
									</article>
								</li>
								@endforeach
							</ul>
							<div class="sortPagiBar">
								<div class="sortPagiBar-inner">
									<nav>
										<ul class="pagination">
											<li class="active"><a href="#">1</a></li>
											<li><a href="#">2</a></li>
											<li><a href="#">3</a></li>
											<li><a href="#">4</a></li>
											<li><a href="#">5</a></li>
											<li>
												<a href="#" aria-label="Next">
													<span aria-hidden="true">Дараах »</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
@include('frontend.partials.footer')
@stop