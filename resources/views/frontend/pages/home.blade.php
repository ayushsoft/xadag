@extends('frontend.app')
@section('content')


@include('frontend.partials.navbar')

<div class="bts-popup" role="alert">
	<div class="bts-popup-container">
		<img style="width: 50%;" src="{{asset('data/option1/khadag.png')}}" alt="" width="80%" />
		<h5>
			Лаклайфын бүтээгдэхүүнүүдийг
		</h5>
		<h3 style="color: #01a74f;">
			Мэд, Мэдээл, Ашиг ол
		</h3>
		<h5 style="padding: 20px;line-height: 35px;">
			Үнийн<span style="font-size: 25px;color:#01a74f;font-weight: 600;">
				10%-ИЙН ХӨНГӨЛӨЛТ
			</span>
			-ийг найзууддаа санал болго.

			
		</h5>
		<h5 style="text-align: center;">
				Таны саналыг хүлээн авсан хүн худалдан авалт хийсэн тохиолдолд та үнийн
			дүнгийн
		</h5>
		
		<h5 style="padding:0 20px 0 20px;line-height: 35px;">
			<span style="font-size: 25px;color:#01a74f;font-weight: 600;">
				10%
			</span>
			-тай тэнцэх урамшууллыг авах эрхтэй болно.

			
		</h5>
		<div class="bts-popup-button" style="margin-top: 20px;">
			<a style="font-size: 25px;color: #f00;border: 1px solid #ff0000;border-radius: 20px;padding: 7px;" href="/regiserpage">Бүртгүүлэх</a>
		</div>

		<a href="#0" class="bts-popup-close img-replace">Хаах</a>

	</div>
</div>

<div class="container">
		<div class="row">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<!-- Block vertical-menu -->
					<div class="block block-vertical-menu">
						<div class="vertical-head">
							<h5 class="vertical-title">Facebook</h5>
						</div>
						<div class="vertical-menu-content">
                            <div class="widget-title"></div>
                            <div class="widget-content">
                                <div class="fb-page" 
                                        data-href="https://www.facebook.com/Luck-Life-Plus-409270776193436/?notif_id=1516796827018323&amp;notif_t=page_fan&amp;ref=notif" 
                                        data-tabs="timeline" 
                                        data-height="390px"
                                        data-small-header="true" 
                                        data-adapt-container-width="true" 
                                        data-hide-cover="false" 
                                        data-show-facepile="true">
                                        <blockquote cite="https://www.facebook.com/Luck-Life-Plus-409270776193436/?notif_id=1516796827018323&amp;notif_t=page_fan&amp;ref=notif" 
                                        class="fb-xfbml-parse-ignore">
                                        <a href="https://www.facebook.com/Luck-Life-Plus-409270776193436/?notif_id=1516796827018323&amp;notif_t=page_fan&amp;ref=notif"></a>
                                        </blockquote>
                                </div>   
                            
                            <div id="fb-root"></div>  
                         </div>
	                    </div>
					</div>
					<!-- ./Block vertical-menu -->
				</div>
			    
				<!--<div class="col-sm-4 col-md-3 wrap-block-cl">-->
				<!--	<div class="inner-cl box-radius" style="border: none;">-->
				<!--		<div class="block-social">-->
    <!--                        <ul class="list-social">-->
    <!--                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
    <!--                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
    <!--                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
    <!--                            <li><a href="#"><i class="fa fa-pied-piper"></i></a></li>-->
    <!--                        </ul>-->
    <!--                    </div>-->
				<!--	</div>-->
				<!--</div>-->
				<!-- ./block cl-->
				<div class="col-sm-9 col-md-6">
					<!-- Home slide -->
					<div class="block block-slider">
						<ul class="home-slider kt-bxslider">
							<li>
								<img src="{{asset('data/option2/slider1.png')}}" alt="Slider">
								<div class="slide-text">
									{{--<h4>--}}
										{{--ОРОЛЦОГЧ БҮХ ТАЛ СЭТГЭЛ ХАНГАЛУУН БАЙХ--}}
										{{--ОРЧИН ҮЕИЙН МАРКЕТИНГТ ХАМТДАА ...--}}
									{{--</h4>--}}
								</div>
							</li>
						</ul>
					</div>
					<!-- ./Home slide -->
				</div>
				<div class="col-sm-9 col-md-3">
					<div class="block-banner-right banner-hover">
						<iframe width="280" height="215" src="https://www.youtube.com/embed/WiZb0V6XaK8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						<iframe width="280" height="215" src="https://www.youtube.com/embed/kcOIYHngZNI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
				</div>
				<!-- block banner owl-->
				<div class="col-sm-12">
					<div class="block block-tabs">
						<div class="block-head">
							<div class="block-title">
								<div class="block-title-text text-lg">Мэдээ мэдээлэл</div>
							</div>
							<ul class="nav-tab">                                   
		                        <li><a data-toggle="tab" href="#tab-2">Бүгд</a></li>
		                        <li  class="active"><a data-toggle="tab" href="#tab-1">Онцлох</a></li>
		                        <li><a data-toggle="tab" href="#tab-2">Сүүлд нэмэгдсэн</a></li>
		                        <li><a data-toggle="tab" href="#tab-1">Их уншсан</a></li>
		                        
	                      	</ul>   
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1" class="tab-panel active">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										@foreach($news as $n)
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="uploads/news/{{$n->image}}" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">{!! $n->title !!}</a>
													</div>
													<!--<div class="price-box">-->
													<!--	<span class="product-price">$139.98</span>-->
													<!--	<span class="product-price-old">$169.00</span>-->
													<!--</div>-->

				                                    <div class="product-button">
														<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Дэлгэрэнгүй<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										@endforeach
									</ul>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<!-- ./block banner owl-->
				<!-- block tabs -->
				<div class="col-sm-12">
					<div class="block block-tabs">
						<div class="block-head">
							<div class="block-title">
								<div class="block-title-text text-lg">Бүтээгдэхүүнүүд</div>
							</div>
							<ul class="nav-tab">                                   
		                    
		                        <li  class="active"><a data-toggle="tab" href="#tab-1">Нөхөн сэргээх</a></li>
		                        <li><a data-toggle="tab" href="#tab-2">Бие эрхтэн цэвэрлэх</a></li>
		                        <li><a data-toggle="tab" href="#tab-1">Дархлаа дэмжих</a></li>
		                        
	                      	</ul>   
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-1" class="tab-panel active">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										@foreach($product as $p)
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
                                                        @if(Auth::check())
                                                            <a class="product-img" href="{{url('products/'.$p->id.'/'.Auth::user()->code)}}">
                                                                <img src="uploads/product/{{$p->image}}" alt="Product">
                                                            </a>
                                                        @else
														    <a class="product-img" href="{{url('products',$p->id)}}"><img src="uploads/product/{{$p->image}}" alt="Product"></a>
                                                        @endif
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
                                                        @if(Auth::check())
                                                            <a href="{{url('products/'.$p->id.'/'.Auth::user()->code)}}">{!! $p->name !!}</a>
                                                        @else
														    <a href="{{url('products',$p->id)}}">{!! $p->name !!}</a>
                                                        @endif
													</div>
													<div class="price-box">
														<span class="product-price">{!! $p->price !!} ₮</span>
														<span class="product-price-old">{!! $p->price !!} ₮</span>
													</div>
													{{--<div class="product-star">--}}
				                                        {{--<i class="fa fa-star"></i>--}}
				                                        {{--<i class="fa fa-star"></i>--}}
				                                        {{--<i class="fa fa-star"></i>--}}
				                                        {{--<i class="fa fa-star"></i>--}}
				                                        {{--<i class="fa fa-star-half-o"></i>--}}
				                                    {{--</div>--}}
				                                    <div class="product-button">

				                                    	@if(Auth::check())
															<a class="button-radius btn-add-cart" title="Add to Cart" href="{{url('products/'.$p->id.'/'.Auth::user()->code)}}">Дэлгэрэнгүй<span class="icon"></span></a>
														@else
															<a class="button-radius btn-add-cart" title="Add to Cart" href="{{url('products',$p->id)}}">Дэлгэрэнгүй<span class="icon"></span></a>
														@endif
				                                    </div>
												</div>
											</div>
										</li>
										@endforeach
									</ul>
								</div>
								<!-- <div id="tab-2" class="tab-panel">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p6.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p7.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p8.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p9.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p10.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star"></i>
				                                        <i class="fa fa-star-half-o"></i>
				                                    </div>
				                                    <div class="product-button">
				                                    	<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
				                                    	<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
				                                    	<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
				                                    </div>
												</div>
											</div>
										</li>
									</ul>
								</div> -->
							</div>
						</div>
					</div>
				</div>
				<!-- ./block tabs -->
				<div class="col-sm-12">
					<div class="block 18 block-tabs">
						<div class="block-head">
							<div class="block-title">
								<div class="block-title-text text-lg">Шинээр нэмэгдсэн</div>
							</div>
							<!-- <ul class="nav-tab">
		                        <li><a data-toggle="tab" href="#tab-4">All</a></li>
		                        <li  class="active"><a data-toggle="tab" href="#tab-3">Beauty & Perfumes</a></li>
		                        <li><a data-toggle="tab" href="#tab-4">Mobile & Tablets</a></li>
		                        <li><a data-toggle="tab" href="#tab-3">Fashion</a></li>
		                        <li><a data-toggle="tab" href="#tab-4">Auto Accessories</a></li>
	                      	</ul> -->
						</div>
						<div class="block-inner">
							<div class="tab-container">
								<div id="tab-3" class="tab-panel active">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										@foreach($last as $p)
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															@if(Auth::check())
																<a class="product-img" href="{{url('products/'.$p->id.'/'.Auth::user()->code)}}"><img src="uploads/product/{{$p->image}}" alt="Product"></a>
															@else
																<a class="product-img" href="{{url('products',$p->id)}}"><img src="uploads/product/{{$p->image}}" alt="Product"></a>
															@endif
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															@if(Auth::check())
																<a href="{{url('products/'.$p->id.'/'.Auth::user()->code)}}">{!! $p->name !!}</a>
															@else
																<a href="{{url('products',$p->id)}}">{!! $p->name !!}</a>
															@endif
														</div>
														<div class="price-box">
															<span class="product-price">{!! $p->price !!} ₮</span>
															<span class="product-price-old">{!! $p->price !!} ₮</span>
														</div>
														{{--<div class="product-star">--}}
														{{--<i class="fa fa-star"></i>--}}
														{{--<i class="fa fa-star"></i>--}}
														{{--<i class="fa fa-star"></i>--}}
														{{--<i class="fa fa-star"></i>--}}
														{{--<i class="fa fa-star-half-o"></i>--}}
														{{--</div>--}}
														<div class="product-button">

                                                            @if(Auth::check())
                                                                <a class="button-radius btn-add-cart" title="Add to Cart" href="{{url('products/'.$p->id.'/'.Auth::user()->code)}}">
																	Дэлгэрэнгүй<span class="icon"></span></a>
                                                            @else
                                                                <a class="button-radius btn-add-cart" title="Add to Cart" href="{{url('products',$p->id)}}">Дэлгэрэнгүй<span class="icon"></span></a>
                                                            @endif
														</div>
													</div>
												</div>
											</li>
										@endforeach
									</ul>
								</div>
								<div id="tab-4" class="tab-panel">
									<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p16.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
													</div>
													<div class="product-button">
														<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
														<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
														<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
													</div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p17.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
													</div>
													<div class="product-button">
														<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
														<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
														<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
													</div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p18.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
													</div>
													<div class="product-button">
														<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
														<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
														<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
													</div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p19.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
													</div>
													<div class="product-button">
														<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
														<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
														<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
													</div>
												</div>
											</div>
										</li>
										<li class="product">
											<div class="product-container">
												<div class="product-left">
													<div class="product-thumb">
														<a class="product-img" href="#"><img src="data/option2/p20.jpg" alt="Product"></a>
														<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
													</div>
												</div>
												<div class="product-right">
													<div class="product-name">
														<a href="#">Cotton Lycra Leggings</a>
													</div>
													<div class="price-box">
														<span class="product-price">$139.98</span>
														<span class="product-price-old">$169.00</span>
													</div>
													<div class="product-star">
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star"></i>
														<i class="fa fa-star-half-o"></i>
													</div>
													<div class="product-button">
														<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
														<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
														<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Block hot deals2 -->
				@if(Auth::check())
				<div class="col-sm-12">
						<div class="block 18 block-tabs">
							<div class="block-head">
								<div class="block-title">
									<div class="block-title-text text-lg">Миний сүүлд үзсэн бүтээгдэхүүн</div>
								</div>
								<!-- <ul class="nav-tab">
                                    <li><a data-toggle="tab" href="#tab-4">All</a></li>
                                    <li  class="active"><a data-toggle="tab" href="#tab-3">Beauty & Perfumes</a></li>
                                    <li><a data-toggle="tab" href="#tab-4">Mobile & Tablets</a></li>
                                    <li><a data-toggle="tab" href="#tab-3">Fashion</a></li>
                                    <li><a data-toggle="tab" href="#tab-4">Auto Accessories</a></li>
                                  </ul> -->
							</div>
							<div class="block-inner">
								<div class="tab-container">
									<div id="tab-3" class="tab-panel active">
										<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p11.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p12.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p13.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p14.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p15.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div id="tab-4" class="tab-panel">
										<ul class="products kt-owl-carousel" data-margin="20" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":5}}'>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p16.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p17.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p18.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p19.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
											<li class="product">
												<div class="product-container">
													<div class="product-left">
														<div class="product-thumb">
															<a class="product-img" href="#"><img src="data/option2/p20.jpg" alt="Product"></a>
															<a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
														</div>
													</div>
													<div class="product-right">
														<div class="product-name">
															<a href="#">Cotton Lycra Leggings</a>
														</div>
														<div class="price-box">
															<span class="product-price">$139.98</span>
															<span class="product-price-old">$169.00</span>
														</div>
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
														<div class="product-button">
															<a class="btn-add-wishlist" title="Add to Wishlist" href="#">Add Wishlist</a>
															<a class="btn-add-comparre" title="Add to Compare" href="#">Add Compare</a>
															<a class="button-radius btn-add-cart" title="Add to Cart" href="#">Buy<span class="icon"></span></a>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif

			</div>
		</div>
	</div>
@include('frontend.partials.footer')
@stop