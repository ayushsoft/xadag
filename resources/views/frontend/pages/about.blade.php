@extends('frontend.app')
@section('content')

    @include('frontend.partials.navbar')
    <div class="container">
        <div class="row">
            <div class="block block-breadcrumbs">
                <ul>
                    <li class="home">
                        <a href="#"><i class="fa fa-home"></i></a>
                        <span></span>
                    </li>
                    <li>ХАДАГ ЦЭГ КОМ?</li>
                </ul>
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-6">
                    <h1 class="page-title">ХАДАГ ЦЭГ КОМ?</h1>
                    <div class="main-page">
                        <div class="page-content clearfix">

                            <p>
                                Өдрөөс өдөрт, жилээс жилд нэмэгдэж байгаа байгаль орчны бохирдол, нийгэм,
                                эдийн засгийн үйл явдлын хурд, стресс зэргийн сөрөг нөлөөллийг бууруулахын тулд
                                амьдралын зөв хэв маяг, дадалтай болохын зэрэгцээ биед шаардлагатай эрдэс бодисоо
                                органик аргаар гарган авсан хүнсний нэмэлт бүтээгдэхүүнээр нөхөн авах шаардлага
                                тулгардаг.
                            </p>
                            <p>
                                Иймээс “ЛАКЛАЙФ-ИХ МОНГОЛ” ХХК нь “ХАДАГ ЦЭГ КОМ”-оор дамжуулан та бүхэнд дараах хоёр
                                боломжийг санал болгож байна.Үүнд:
                            </p>
                            <p>
                                <b>1.</b> Олон хүн “LUCKLIFE” компаний дэлхийд тэргүүлэх, дэвшилтэт технологиор
                                хийгдсэн бүтээгдэхүүнүүдийг хэрэглээд, богино хугацаанд үр дүнг нь мэдэрч,
                                эрүүл, аз жаргалтай болж байдаг. Та бүхэн ч гэсэн тэдний нэгэн адилаар эдгээр
                                гайхалтай бүтээгдэхүүнүүдийг хайртай бүхэнтэйгээ хамт хэрэглэн, ЭРҮҮЛ ЭНХ
                                байх боломжтой.<br>
                                <b>2. </b>Бидний санал болгох дараагийн зүйл бол “LUCKLIFE” компанийн маркетингийн
                                өвөрмөц систем юм. Та бүхэн эдгээр шилдэг бүтээгдэхүүнүүдийг сайтар
                                судалсны үндсэн дээр өвдөж шаналсан эсхүл илүү эрүүл чийрэг, аз жаргалтай
                                байхыг хүссэн хэрэглэгчдэд санал болгон НЭМЭЛТ ОРЛОГО олох боломжтой.
                                Компани дээр явагддаг сургалтуудад үнэ төлбөргүй хамрагдах, мэдлэгээ тэлэх
                                боломжийг хамтад нь олгоно.
                            </p>
                        </div>
                    </div>


                    <div class="col-sm-8 col-md-12" style="border-top: 1px solid #04a54d; margin-top:40px;">
                        <h4 style="text-align: center;margin-top: 50px;line-height: 25px;">
                            Эрүүл мэнд, эд баялгийг <br>Цэнхэр хадагтай өргөн барьж байна.
                        </h4>
                        <h4 style="text-align: center;margin: 25px 0;color: #175ca9;">
                            МЭД, МЭДСЭНЭЭ МЭДЭЭЛЖ, АШИГ ОЛ <br><br><br>
                            <a style="color:#ff0000;font-size: 24px;text-align: center;
                                     border: 1px solid #ff0000;border-radius: 20px;padding: 7px;" href="/regiserpage">БҮРТГҮҮЛЭХ
                                ҮҮ</a>
                        </h4>

                    </div>

                </div>
                <div class="col-sm-6 col-md-6">

                    <div class="block block-top-sellers" style="border: none;box-shadow: none">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/1.jpg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/2.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/3.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/4.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/5.jpg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/6.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/7.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/8.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/9.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/10.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/11.jpeg')}}">
                                </div> <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/12.jpeg')}}">
                                </div>

                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/13.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/14.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/15.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/16.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/17.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/18.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/19.jpeg')}}">
                                </div>
                                <div class="swiper-slide">
                                    <img src="{{asset('data/aboutslide/20.jpg')}}">
                                </div>

                            </div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('frontend.partials.footer')

@stop