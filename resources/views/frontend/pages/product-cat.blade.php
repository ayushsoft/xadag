@extends('frontend.app')
@section('content')
    <?php function dazo_substr_small($txt='',$limit = 141){
        $buf = '';
        $k=0; //үсгийн байрлалыг тодорхойлоход ашиглагдана.

        for($i=0;$i<strlen($txt);$i++){

            $k++;

            $buf .= $txt{$i};
            if(ord($txt{$i})>207 && ord($txt{$i})<212){ //крилл үсэг үсэг байх тохиолдолд хийх үйлдэл
                $i++;
                $buf .= $txt{$i};
            }
            if($k>=$limit){ //авах үсгийн хязгаарт хүрмэгч зогсоно
                return $buf;
            }
        }
        return $buf;
    }

    ?>


    @include('frontend.partials.navbar')

    <div class="container">
        <div class="row">
            <div class="block block-breadcrumbs">
                <ul>
                    <li class="home">
                        <a href="#"><i class="fa fa-home"></i></a>
                        <span></span>
                    </li>
                    <li><a href="{{url('products-grid')}}">Бүтээгдэхүүнүүд</a><span></span></li>
                    <li class="active">{{$producttype->name}}</li>
                </ul>
            </div>

            <h3 class="page-title">
                <p style="font-weight: 300;">Бүтээгдэхүүнүүд /&nbsp;<b>{{$producttype->name}}</b></p>
            </h3>

            <div class="category-products">
                <ul class="products list row">
                    @foreach($product  as $p)
                        <li class="product col-xs-12 col-sm-6">
                            <div class="product-container">
                                <div class="inner row">
                                    <div class="product-left col-sm-6">
                                        <div class="product-thumb">
                                            <a class="product-img" href="{{url('products',$p->id)}}"><img src="/uploads/product/{{$p->image}}" alt="Product"></a>
                                            <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                        </div>
                                    </div>
                                    <div class="product-right col-sm-6">
                                        <div class="product-name">
                                            <a href="{{url('products',$p->id)}}">{!! dazo_substr_small($p->name,25) !!}...</a>

                                        </div>
                                        <div class="price-box">
                                            <span class="product-price">{!! $p->price !!}</span>
                                            <span class="product-price-old">{!! $p->price !!}</span>
                                        </div>
                                        <div class="product-star">
                                            {{$p->producttype->name}}
                                        </div>
                                        <div class="desc">
                                            {!! dazo_substr_small($p->zaavar,70) !!}...
                                        </div>
                                        <div class="product-button">
                                            <a class="btn-add-wishlist" title="Хүслийн жагсаалтанд нэмэх" href="#">Хүслийн жагсаалтанд нэмэх</a>
                                            <a class="button-radius btn-add-cart" title="Add to Cart" href="{{url('products',$p->id)}}">Худалдаж авах<span class="icon"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>
    </div>

    @include('frontend.partials.footer')
@stop