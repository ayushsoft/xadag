@extends('frontend.app')
@section('content')

    @include('frontend.partials.navbar')

    <div class="container marketing">
        <div class="block block-breadcrumbs" style="margin-bottom: 30px;">
            <ul>
                <li class="home">
                    <a href="#"><i class="fa fa-home"></i></a>
                    <span></span>
                </li>
                <li>Хамтран ажиллах хялбар алхамууд</li>
            </ul>
        </div>
        <!-- Three columns of text below the carousel -->
        <div class="row" style="margin-bottom: 50px;">
            <div class="title" style="margin-left: 20px;padding: 30px 0;">
                <h4 style="color:#175ca9;">
                    Худалдан авалтын урамшуулал
                </h4>
            </div>
            <div class="col-lg-3 working-image">

                <div class="working-page">
                    <div class="working-img">
                        <i style="font-size: 30px;margin: 30% 0 0 30%;color: #fff;" class="fa fa-user" aria-hidden="true"></i>
                    </div>

                </div>



                <h4>1. ХАДАГ ЦЭГ КОМ вебсайтад бүртгүүлэх</h4>

            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3 working-image">
                <div class="working-img">
                    <i style="font-size: 30px;margin: 30% 0 0 30%;color: #fff;" class="fa fa-book" aria-hidden="true"></i>
                </div>

                <h4>
                    2. Бүтээгдэхүүнүүдийн ашиг тусыг мэдэх
                </h4>

            </div>

            <div class="col-lg-3 working-image">
                <div class="working-img">
                    <i style="font-size: 30px;margin: 30% 0 0 26%;color: #fff;" class="fas fa-shopping-cart" aria-hidden="true"></i>
                </div>

                <h4>
                    3. Өөрийн кодыг ашиглан худалдан авалт хийх
                </h4>


            </div>
            <div class="col-lg-3 working-image">
                <div class="working-img">
                    <i style="font-size: 30px;margin: 30% 0 0 36%;color: #fff;" class="fas fa-dollar-sign   " aria-hidden="true"></i>
                </div>

                <h4>
                    4. Сугалаанд оролцох<br>
                    <span>
                        <a href="{{url('bonus')}}" style="color: red;font-size: 15px;">
                            нэмэлт мэдээллийг энд дарж авна уу
                        </a>
                    </span>
                </h4>


            </div>
        </div>
        <div class="row" style="margin-bottom: 50px;">
            <div class="title" style="padding: 30px 0;">
                <h4 style="color:#175ca9;">
                    Найздаа санал болгоод авах урамшуулал
                </h4>
            </div>
            <div class="col-lg-3 working-image">

                <div class="working-page">
                    <div class="working-img">
                        <i style="font-size: 30px;margin: 30% 0 0 30%;color: #fff;" class="fa fa-user" aria-hidden="true"></i>
                    </div>
                    
                </div>

                

                <h4>1. ХАДАГ ЦЭГ КОМ вебсайтад бүртгүүлэх</h4>
                <p>
                     Бүртгүүлснээр та шинэ
                    бүтээгдэхүүний мэдээлэл, үнийн хөнгөлөлт, сургалтууд зэргийн талаар
                    мэдээллийг шуурхай авахаас гадна бүтээгдэхүүнүүдийг бусдад санал
                    болгон ашиг орлого олох боломжтой болно.
                </p>
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-3 working-image">
                <div class="working-img">
                        <i style="font-size: 30px;margin: 30% 0 0 30%;color: #fff;" class="fa fa-book" aria-hidden="true"></i>
                    </div>

                <h4>
                   2. Бүтээгдэхүүнүүдийн талаарх мэдлэгээ дээшлүүлэх
                </h4>
                <p>
                    1. Лаклайф компаниас зохион байгуулах сургалтуудад үнэ
                    төлбөргүй хамрагдах,<br>
                    2. Бүтээгдэхүүнүүдийн талаар уншиж судлах,<br>
                    3. Боломжит бүхий л аргаар илүү ихийг мэдэх,
                </p>
            </div>

            <div class="col-lg-3 working-image">
                <div class="working-img">
                        <i style="font-size: 30px;margin: 30% 0 0 30%;color: #fff;" class="fa fa-share-alt" aria-hidden="true"></i>
                    </div>

                <h4>
                    3. Найздаа санал болгох
                </h4>
                <p>
                    Имэйлээр, сошиал медиагаар болон хөнгөлөлтийн купоныг хэвлэж
                    өөрийн найз нөхдөд бүтээгдэхүүнийг ердийн үнээс
                    <span style="color: #01953f;font-size: 15px;text-transform: uppercase;font-weight: bold;"> 10%-ийн хөнгөлөлттэй</span>-гээр худалдан авахыг санал болгох. Нэвтэрч орсноор таны
                    бүртгэлийн дугаар купон дээр гарч ирнэ,
                </p>

            </div>
            <div class="col-lg-3 working-image">
                <div class="working-img">
                        <i style="font-size: 30px;margin: 30% 0 0 36%;color: #fff;" class="fas fa-dollar-sign   " aria-hidden="true"></i>
                    </div>

                <h4>
                    4. Мөнгөн урамшуулал авах
                </h4>
                <p>
                    Таны санал болгосон хүн бүтээгдэхүүнийг хөнгөлөлттэйгээр худалдан авсан тохиолдолд ТАНД
                    үнийн дүнгийн <span style="color: #01953f;font-size: 15px;font-weight: bold;">10%</span>-тай тэнцэх хэмжээний урамшуулал авах эрх үүснэ.
                </p>

            </div>
        </div>
        <div class="row" style="border-top: 1px solid #ccc;">
            <div class="col-lg-12">
                <h4 style="text-align: center;margin: 50px 0;">
                   МЭД, МЭДСЭНЭЭ МЭДЭЭЛЖ, АШИГ ОЛ <a style="color: red;font-weight: bold;
                    border:1px solid #ff0000;border-radius: 8px;padding: 4px;" href="/regiserpage">БҮРТГҮҮЛЭХ ҮҮ</a>
                </h4>
            </div>
        </div>
    </div>

    @include('frontend.partials.footer')
@stop