@extends('frontend.app')
@section('content')


    @include('frontend.partials.navbar')
    <div class="container">
        <div class="row">
            <div class="block block-breadcrumbs">
                <ul>
                    <li class="home">
                        <a href="#"><i class="fa fa-home"></i></a>
                        <span></span>
                    </li>
                    <li>Холбоо барих</li>
                </ul>
            </div>
            <div class="main-page">
                <h1 class="page-title">Холбоо барих</h1>
                <div class="page-content contact-page">
                    <div id="message-box-conact"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Санал хүсэлт илгээх</h4>
                            <div>

                                <p>
                                    <label>И-мэйл хаяг</label>
                                    <input id="address" type="text">
                                </p>
                                <p>
                                    <label>Нэр</label>
                                    <input id="order_reference" type="text">
                                </p>
                                <p>
                                    <label>Тайлбар</label>
                                    <textarea id="message" rows="5"></textarea>
                                </p>
                                <p>
                                    <button class="button" id="btn-send-contact">Илгээх</button>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h4>Хаяг байршил</h4>
                            <div>

                                <ul class="store_info">
                                    <li><i class="fa fa-home"></i>Хаяг: 44-33, БГД, Улаанбаатар</li>
                                    <li><i class="fa fa-phone"></i><span>Утас: ( +976 ) 99030783</span></li>

                                    <li><i class="fa fa-envelope"></i><span><a href="#">Имэйл: info@xadag.com</a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @include('frontend.partials.footer')
@stop