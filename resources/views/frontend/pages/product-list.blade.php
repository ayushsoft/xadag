@extends('frontend.app')
@section('og-url'){!!$product->id.'/'.$code!!}@stop
@section('og-title'){{$product->name}} @stop
@section('og-description'){{'Манай сайтын '.$code.' кодтой'}} хэрэглэгч энэхүү бүтээгдэхүүнийг 10%-ийн хөнгөлөлттэйгээр худалдан авахыг санал болгож байна.
@stop
@section('og-image'){!!$product->image!!}@stop
@section('content')

    @include('frontend.partials.navbar')


    <!-- <div class="bts-popup" role="alert">
	<div class="bts-popup-container">
		<img src="{{asset('data/option1/logo.jpg')}}" alt="" width="80%" />
		<a href="#0" class="bts-popup-close img-replace">Хаах</a>
	</div>
</div> -->

    <div class="container product-page">
        <div class="row">
            <div class="block block-breadcrumbs">
                <ul>
                    <li class="home">
                        <a href="#"><i class="fa fa-home"></i></a>
                        <span></span>
                    </li>
                    <li><a href="#">Бүтээгдэхүүнүүд</a><span></span></li>
                </ul>
            </div>
        </div>
        @if(Auth::check())
        @endif
        <div class="row">
            <div class="row">
                <div class="col-sm-5">
                    <div class="block block-product-image">
                        <div class="product-image easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                            <a href="/uploads/product/{{$product->image}}">
                                <img src="/uploads/product/{{$product->image}}" alt="Product" width="450" height="450"/>
                            </a>
                        </div>
                        <div class="text">Hover on the image to zoom</div>
                        <div class="product-list-thumb">
                            <ul class="thumbnails kt-owl-carousel" data-margin="10" data-nav="true"
                                data-responsive='{"0":{"items":2},"600":{"items":2},"1000":{"items":3}}'>
                                @foreach($productimgs as $productimg)
                                    <li>
                                        <a class="selected" href="/app/{{$productimg->filename}}"
                                           data-standard="/app/{{$productimg->filename}}">
                                            <img src="/app/{{$productimg->filename}}" alt=""/>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-12 col-md-7">
                            <div class="block-product-info">
                                <h3 class="product-name">{{$product->name}} </h3>
                                <div class="price-box">
                                    <span class="product-price">{{$product->price}} ₮</span>
                                    {{--<span class="product-price-old">{{$product->price}}</span>--}}
                                </div>

                                <div class="desc">
                                    {{$product->zaavar}}
                                </div>
                                <div class="variations-box">
                                    <table class="variations-table">
                                        <tr>
                                            <td class="table-label">Хадгалах хугацаа</td>
                                            <td class="table-value">
                                                {{$product->save_time}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-label">Савалгаа</td>
                                            <td class="table-value">
                                                <ul class="list-check-box">
                                                    <li><a href="#">{{$product->savalgaa}}</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="table-label">тоо:</td>
                                            <td class="table-value">
                                                <div class="box-qty">
                                                    <a href="#" class="quantity-minus">-</a>
                                                    <input type="text" class="quantity" value="1">
                                                    <a href="#" class="quantity-plus">+</a>
                                                </div>
                                                @if(Auth::check())
                                                    <form action="/user/cart/add" method="post" name="add_to_cart">
                                                        {!! csrf_field() !!}
                                                        <input type="hidden" name="product" value="{{$product->id}}"/>
                                                        <input type="hidden" name="qty" value="1"/>
                                                        <button class="button-radius btn-add-cart"><span class="icon">
														</span>Сагсанд нэмэх
                                                        </button>
                                                    </form>
                                                @else
                                                    <a href="{{url('loginpage')}}" class="button-radius btn-add-cart">
                                                        Сагсанд нэмэх<span class="icon">
													</span>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="box-control-button">
                                    <!-- <a class="link-wishlist" href="#">wishlist</a>
                                    <a class="link-compare" href="#">Compare</a>
                                    <a class="link-sendmail" href="#">Email to a Friend</a>
                                    <a class="link-print" href="#">Print</a> -->
                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            {{Session::get('success')}}
                                        </div>
                                    @endif

                                    <div class="page-header">
                                        <h4>"{{$product->name}} "</h4>
                                    </div>

                                    <p> Бүтээгдэхүүнийг найздаа санал болгоод үнийн дүнгийн 10%-ийг аваарай</p>


                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-5">
                            <!-- block  top sellers -->
                            <div class="block block-top-sellers" id="print" style="border: 0.6px solid #01953f;">
                                <div class="block-head" style="background: #57e492;">
                                    <div class="block-title">
                                        <div class="block-icon">
                                            <img src="{{asset('data/top-seller-icon.png')}}" alt="store icon">
                                        </div>
                                        <div class="block-title-text text-sm">Мэд, Мэдээл</div>
                                        <div class="block-title-text text-lg"> Ашиг ол</div>
                                    </div>
                                </div>
                                <div class="block-inner">

                                    <div class="block-inner">
                                        <div class="product">
                                            <div class="product-name">
                                                <h5 style="line-height: 20px;">
                                                    <span style="color: #01953f;font-weight: bold;">10%-ИЙН ХӨНГӨЛӨЛТ</span>-ийг
                                                    санал болгоод, үнийн дүнгийн <span
                                                            style="text-transform: uppercase;color: red;font-weight: bold;">
														10%-тай
													тэнцэх урамшуулал</span>-ыг аваарай.
                                                </h5>
                                            </div>
                                            <div class="product-name">
                                                <h5 style="line-height: 20px;">
                                                    Таны санал болгосон хүн худалдан авалт хийснээр та урамшуулал авах
                                                    эрхтэй болно.
                                                </h5>
                                            </div>

                                            <div class="image">
                                                <a href="#"><img src="/uploads/product/{{$product->image}}"
                                                                 alt="p23.jpg"></a>
                                            </div>
                                            <div class="product-name" style="margin-top: 10px;">
                                                <a style="color: #01953f;text-align: center!important;font-weight: bold;"
                                                   href="#">
                                                    {{$product->name}}</a>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price">{{$product->price}}</span>
                                                <span class="product-price-old">{{$product->price}}</span>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price" style="color: #000;font-weight: 400;">Санал болгогч хүний код:</span>
                                                <span class="product-price" style="color: red;margin-left: 35%;">
													@if(Auth::check())
                                                        {{Auth::user()->code}}
                                                    @endif
												</span>
                                            </div>
                                            <div class="price-box">
                                                <span class="product-price"></span>

                                            </div>
                                            <div class="product-name">
                                                <h5 style="line-height: 20px;">
                                                    Энэхүү тасалбарыг үзүүлэгч нь бүтээгдэхүүний анхны үнийн<br>
                                                    <b style="color: #0a6aa1">10%-ийн</b> хөнгөлөлт эдэлнэ.
                                                </h5>
                                            </div>
                                        </div>
                                        <div id="fb-root"></div>
                                        @if(Auth::check())

                                            <div class="col-sm-6">
                                                <div style="margin-bottom: 6px;" class="fb-share-button"
                                                     data-href="http://xadag.com/products/3/LL000004"
                                                     data-layout="button_count" data-size="small"
                                                     data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore"
                                                                                  target="_blank"
                                                                                  href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fxadag.com%2Fproducts%2F3%2FLL000004&amp;src=sdkpreparse">share</a>
                                                </div>

                                                <a href="https://twitter.com/share?ref_src=twsrc%5Etfw"
                                                   class="twitter-share-button" data-text="Лаклайф" data-via="Лаклайф"
                                                   data-hashtags="Лаклайф" data-related="Лаклайф,Лаклайф" data-lang="en"
                                                   data-show-count="false">Жиргэх</a>
                                                <script async src="https://platform.twitter.com/widgets.js"
                                                        charset="utf-8"></script>
                                            </div>


                                            <div class="col-sm-6" style="padding:0 10px;">
                                                <input style="width: 100%;margin-bottom: 4px;height: 20px;margin-top: 3px;"
                                                       class="btn btn-primary btn-xs" type="button"
                                                       onclick="printDiv('block-top-sellers')" value="хэвлэх"/>
                                                <a style="background: #0084ff;color: #fff;padding: 2px 2px 2px 5px;border-radius: 2px;"
                                                   href="https://www.facebook.com/dialog/send?app_id=237722380007266&link=http://www.xadag.com/products/{{$product->id}}/{!!Auth::User()->code!!}&redirect_uri=https://facebook.com">
                                                    <i class="fab fa-facebook-messenger"></i>&nbsp;messenger</a>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="email-send">
                                                    <a href="{{url('email/send',$product->id)}}">email</a>
                                                </div>
                                            </div>
                                        @else
                                            <p>
                                                Уучлаарай та хэрэглэгчийн эрхээр нэвтэрч хөнгөлөлт эдлээрэй.
                                            </p>
                                            <a style="color: #01953f;" href="/loginpage">Нэвтрэх</a>
                                            <a style="color: #01953f;float: right;" href="/regiserpage">Бүртгүүлэх</a>

                                        @endif
                                    </div>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>


            <!-- Product tab -->
            <div class="block block-tabs tab-left">
                <div class="block-head">
                    <ul class="nav-tab">
                        <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">Танилцуулга</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="false">Үйлчилгээ</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="false">орц найрлага</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false">Анхаарах зүйлс</a></li>

                    </ul>
                </div>
                <div class="block-inner">
                    <div class="tab-container">
                        <div id="tab-1" class="tab-panel active">
                            <p>
                                {{$product->description}}
                            </p>
                        </div>
                        <div id="tab-2" class="tab-panel">
                            <p>
                                {{$product->info}}
                            </p>
                        </div>
                        <div id="tab-3" class="tab-panel">
                            <p>
                                {{$product->orts}}
                            </p>
                        </div>
                        <div id="tab-4" class="tab-panel">
                            <p>
                                {{$product->horigloh}}
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="block block-tabs tab-left">
                <div class="block-head">
                    <ul class="nav-tab">
                        <li class="active "><a data-toggle="tab" href="#tab-1">Хэрэглэгчийн сэтгэгдэл</a></li>
                        <!-- <li><a data-toggle="tab" href="#tab-2">Additional</a></li>
                        <li><a data-toggle="tab" href="#tab-3">Reviews</a></li> -->
                    </ul>
                </div>
                <div class="block-inner">
                    <div class="tab-container">
                        <div id="tab-3" class="tab-panel active">
                            <div id="reviews">

                                <ol class="comment-list">
                                    @foreach($commends as $commend)
                                        <li class="comment">
                                            <div class="comment-avatar">
                                                <img src="{{asset('data/avatar.jpg')}}" alt="Avatar">
                                            </div>
                                            <div class="comment-content">
                                                <div class="comment-meta">
                                                    <a href="#" class="comment-author">{{$commend->user->name}}</a>
                                                    <span class="comment-date">{{$commend->created_at}}</span>
                                                    <div class="review-rating">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-half-o"></i>
                                                    </div>
                                                </div>
                                                <div class="comment-entry">
                                                    <p>{{$commend->name}}</p>
                                                </div>
                                                @guest

                                                @else
                                                    @if(Auth::user()->id == $commend->user->id)

                                                        <div class="comment-actions">
                                                            <a class="comment-reply-link"
                                                               href="{{url('/deletecommend',$commend->id)}}"><i
                                                                        class="fa fa-trash-o"></i> Устгах</a>
                                                        </div>
                                                    @endif
                                                @endguest

                                            </div>
                                        </li>
                                    @endforeach

                                </ol>
                                <div class="comment-form">
                                    <h3 class="comment-reply-title">Бүтээгдэхүүнд санал оруулах</h3>
                                    <small>Та санал оруулахын тулд сайтад нэвтэрсэн байх шаардлагатай</small>
                                    @guest

                                        <p>
                                            <label class="required">Name</label>
                                            <input type="text">
                                        </p>

                                        <p>
                                            <label class="required">Comment</label>
                                            <textarea rows="5"></textarea>
                                        </p>
                                        <p>

                                            <a class="button" href="{{route('commend.create')}}">Санал оруулах</a>
                                        </p>

                                    @else
                                        {{Form::open(['route'=>'commend.store'])}}

                                        <p>
                                            <label class="required">Name</label>
                                            <input type="text" value="{{Auth::user()->name}}">
                                        </p>
                                        <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        <p>
                                            <label class="required">Comment</label>
                                            <textarea rows="5" name="name" required></textarea>
                                        </p>
                                        <p>
                                            <input type="submit" class="button" value="Сэтгэгдэл оруулах">
                                        </p>
                                        {{Form::close()}}

                                    @endguest
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    @include('frontend.partials.footer')
@stop
