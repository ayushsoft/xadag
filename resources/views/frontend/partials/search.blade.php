@extends('frontend.app')
@section('content')


    @include('frontend.partials.navbar')
    <?php function dazo_substr_small($txt='',$limit = 141){
        $buf = '';
        $k=0; //үсгийн байрлалыг тодорхойлоход ашиглагдана.

        for($i=0;$i<strlen($txt);$i++){

            $k++;

            $buf .= $txt{$i};
            if(ord($txt{$i})>207 && ord($txt{$i})<212){ //крилл үсэг үсэг байх тохиолдолд хийх үйлдэл
                $i++;
                $buf .= $txt{$i};
            }
            if($k>=$limit){ //авах үсгийн хязгаарт хүрмэгч зогсоно
                return $buf;
            }
        }
        return $buf;
    }

    ?>
    <div class="container">
        <div class="row">
            <div class="block block-breadcrumbs">
                <ul>
                    <li class="home">
                        <a href="#"><i class="fa fa-home"></i></a>
                        <span></span>
                    </li>
                    <li><a href="#">Бүтээгдэхүүнүүд</a><span></span></li>

                </ul>
            </div>

            <h3 class="page-title">
                <span>Бүтээгдэхүүнүүд</span>
            </h3>
            <div class="sortPagiBar">
                <h6>
                    Хайлтын үр дүн: <b>{{ $query }}</b>
                </h6>
                <div class="sortPagiBar-inner">
                    <nav>
                        <ul class="pagination">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">Next »</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    {{--<div class="show-product-item">--}}
                    {{--<select class="">--}}
                    {{--<option value="1">Show 6</option>--}}
                    {{--<option value="1">Show 12</option>--}}
                    {{--</select>--}}
                    {{--</div>--}}

                    {{--<div class="sort-product">--}}
                    {{--<select>--}}
                    {{--<option value="1">Postion</option>--}}
                    {{--<option value="1">Product name</option>--}}
                    {{--</select>--}}
                    {{--<div class="icon"><i class="fa fa-sort-alpha-asc"></i></div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="category-products">
                <ul class="products list row">
                    @if (count($search) === 0)
                    @elseif (count($search) >= 1)
                         @foreach($search  as $p)
                        <li class="product col-xs-12 col-sm-6">
                            <div class="product-container">
                                <div class="inner row">
                                    <div class="product-left col-sm-6">
                                        <div class="product-thumb">
                                            <a class="product-img" href="{{url('products',$p->id)}}"><img src="/uploads/product/{{$p->image}}" alt="Product"></a>
                                            <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                        </div>
                                    </div>
                                    <div class="product-right col-sm-6">
                                        <div class="product-name">
                                            <a href="{{url('products',$p->id)}}">{!! $p->name !!}</a>
                                        </div>
                                        <div class="price-box">
                                            <span class="product-price">{!! $p->price !!}</span>
                                            <span class="product-price-old">{!! $p->price !!}</span>
                                        </div>
                                        <div class="product-star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-half-o"></i>
                                        </div>
                                        <div class="desc">
                                            {!! dazo_substr_small($p->zaavar,80) !!}...
                                        </div>
                                        <div class="product-button">
                                            <a class="btn-add-wishlist" title="Add to Wishlist" href="#">Хүслийн жагсаалтанд нэмэх</a>

                                            <a class="button-radius btn-add-cart" title="Add to Cart" href="{{url('products',$p->id)}}">Худалдаж авах<span class="icon"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    @endif
                </ul>
            </div>

        </div>
    </div>
    @include('frontend.partials.footer')
@stop