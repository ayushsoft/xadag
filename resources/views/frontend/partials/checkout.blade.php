@extends('frontend.app')
@section('content')


    @include('frontend.partials.navbar')
<div class="container">
    <div class="row">
        <div class="block block-breadcrumbs">
            <ul>
                <li class="home">
                    <a href="#"><i class="fa fa-home"></i></a>
                    <span></span>
                </li>
                <li>Гүйлгээ хийх</li>
            </ul>
        </div>
        <div class="main-page">
            <h1 class="page-title">Гүйлгээ хийх</h1>
            <div class="page-content checkout-page">
                <div class="heading-counter warning">Таны сагсанд нийт:
                    <span style="color: green;">{{ $count }}</span>
                    бүтээгдэхүүн байна
                </div>
                <div class="box-border">
                    <table class="table table-bordered table-responsive cart_summary">
                        <thead>
                        <tr>
                            <th class="cart_product">Нэр</th>
                            <th>Тухай</th>
                            <th>Үдсэн үнэ</th>
                            <th>Хямдарсан үнэ</th>
                            <th>Тоо</th>
                            <th>Нийт</th>
                            <th class="action"><i class="fa fa-trash-o"></i></th>
                        </tr>
                        </thead>
                        @foreach($cart_products as $item)
                            <tbody>
                            <tr>
                                <td class="cart_product">
                                    <a href="#"><img class="img-responsive" src="/uploads/product/{{$item->products->image}}" alt="Product"></a>
                                </td>
                                <td class="cart_description">
                                    <p class="product-name">{{$item->products->name}}</p>
                                    <small class="cart_ref">Хадгалах хугацаа: {{$item->products->save_time}}</small>
                                    <br>
                                    <small><a href="#">Color : Beige</a></small>
                                    <br>
                                    <small><a href="#">Size : S</a></small>
                                </td>
                                <td class="price"><span>{{$item->products->price}}₮</span></td>
                                <td class="price"><span>{{$item->products->price}}₮</span></td>
                                <td class="qty">
                                    <input class="form-control input-sm" type="text" value="1">


                                </td>
                                <td class="price">
                                    <span>{{$item->total}}₮</span>
                                </td>
                                <td class="action">
                                    <a href="#">Delete item</a>
                                </td>
                            </tr>

                            </tbody>
                        @endforeach
                        <tfoot>
                        <tr>
                            <td colspan="3"><strong>Нийт төлөх:</strong></td>
                            <td colspan="2"><strong>{{$cart_total}}₮</strong></td>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="cart_navigation">
                        <a class="button" href="{{url('/user/cart')}}"><i class="fa fa-angle-left"></i> Буцах</a>

                    </div>
                </div>

                <h3 class="checkout-sep">1. Хэрэглэгчийн мэдээлэл</h3>
                <div class="box-border">
                    <ul>
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="first_name" class="required">Овог</label>
                                <input type="text" name="first_name" id="first_name">
                            </div><!--/ [col] -->
                            <div class="col-sm-6">
                                <label for="last_name" class="required">Нэр</label>
                                <input type="text" name="last_name" id="last_name">
                            </div><!--/ [col] -->
                        </li><!--/ .row -->
                        <li class="row">
                            <div class="col-sm-6">
                                <label for="company_name">Утас: </label>
                                <input type="text" name="company_name" id="company_name">
                            </div><!--/ [col] -->
                            <div class="col-sm-6">
                                <label for="email_address" class="required">Имэйл хаяг</label>
                                <input type="text" name="email_address" id="email_address">
                            </div><!--/ [col] -->
                        </li><!--/ .row -->
                        <li class="row">
                            <div class="col-xs-12">

                                <label for="address" class="required">оршин суугаа хаяг</label>
                                <input type="text" name="address" id="address">

                            </div><!--/ [col] -->

                        </li><!-- / .row -->

                        <li class="row">

                            <div class="col-sm-6">
                                <label class="required">Хот</label>
                                <select name="Country">
                                    <option value="USA">Улаанбаатар</option>

                                </select>
                            </div><!--/ [col] -->

                            <div class="col-sm-6">
                                <label class="required">Дүүрэг</label>
                                <select name="Province">
                                    <option value="Alabama">Баянзүрх</option>
                                    <option value="Illinois">Чингэлтэй</option>
                                    <option value="Kansas">Хан-Уул</option>
                                </select>
                            </div><!--/ [col] -->
                        </li><!--/ .row -->




                    </ul>
                </div>

                <h3 class="checkout-sep">2. Төлбөр төлөх заавар</h3>
                <div class="box-border">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Дансаар төлөх</h4>
                            <p>Хаан банк:</p>
                            <p><i class="fa fa-check-circle text-primary"></i>5890718112</p>
                            <p><i class="fa fa-check-circle text-primary"></i>5890718112</p>
                            <br>
                            <h4>Гүйлгээний утга</h4>
                            <p><i class="fa fa-check-circle text-primary"></i>та эхлээд өөрийн хэрэглэгчийн код бичнэ</p>
                            <p><i class="fa fa-check-circle text-primary"></i>Хэрвээ та найз тань санал болгосон бол найзыхаа кодыг бичнэ.</p>

                        </div>
                        <div class="col-sm-6">

                            <p>Жишээ нь:</p>
                            <p>me-LL000001,naiz-LL000004</p>

                            <button class="button">Илгээх</button>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
    @include('frontend.partials.footer')
@stop