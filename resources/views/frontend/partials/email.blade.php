@extends('frontend.app')
@section('content')
    <div class="container">

            <div class="col-md-2"></div>
            <div class="col-md-4">
                <div class="block block-top-sellers" id="print" style="border: 0.6px solid #01953f;">
                    <div class="block-head" style="background: #57e492;">
                        <div class="block-title">
                            <div class="block-icon">
                                <img src="{{asset('data/top-seller-icon.png')}}" alt="store icon">
                            </div>
                            <div class="block-title-text text-sm">Мэд, Мэдээл</div>
                            <div class="block-title-text text-lg"> Ашиг ол</div>
                        </div>
                    </div>
                    <div class="block-inner">

                        <div class="block-inner">
                            <div class="product">
                                <div class="product-name">
                                    <h5 style="line-height: 20px;">
                                        <span style="color: #01953f;font-weight: bold;">10%-ИЙН ХӨНГӨЛӨЛТ</span>-ийг
                                        санал болгоод, үнийн дүнгийн <span
                                                style="text-transform: uppercase;color: red;font-weight: bold;">
														10%-тай
													тэнцэх урамшуулал</span>-ыг аваарай.
                                    </h5>
                                </div>
                                <div class="product-name">
                                    <h5 style="line-height: 20px;">
                                        Таны санал болгосон хүн худалдан авалт хийснээр та урамшуулал авах
                                        эрхтэй болно.
                                    </h5>
                                </div>

                                {{--<div class="image">--}}
                                    {{--<a href="#"><img src="/uploads/product/{!! $product->image !!}"--}}
                                                     {{--alt="p23.jpg"></a>--}}
                                {{--</div>--}}
                                <div class="product-name" style="margin-top: 10px;">
                                    <a style="color: #01953f;text-align: center!important;font-weight: bold;"
                                       href="#">
                                        {!! $product->name !!}</a>
                                </div>
                                {{--<div class="price-box">--}}
                                    {{--<span class="product-price">{!! $product->price !!}</span>--}}

                                {{--</div>--}}
                                {{--<div class="price-box">--}}
                                    {{--<span class="product-price" style="color: #000;font-weight: 400;">Санал болгогч хүний код:</span>--}}
                                    {{--<span class="product-price" style="color: red;margin-left: 35%;">--}}
													{{--@if(Auth::check())--}}
                                            {{--{{Auth::user()->code}}--}}
                                        {{--@endif--}}
												{{--</span>--}}
                                {{--</div>--}}
                                <div class="price-box">
                                    <span class="product-price"></span>

                                </div>
                                <div class="product-name">
                                    <h5 style="line-height: 20px;">
                                        Энэхүү тасалбарыг үзүүлэгч нь бүтээгдэхүүний анхны үнийн<br>
                                        <b style="color: #0a6aa1">10%-ийн</b> хөнгөлөлт эдэлнэ.
                                    </h5>
                                </div>
                            </div>
                            <div id="fb-root"></div>

                        </div>

                    </div>
                </div>


            </div>

            <div class="col-md-4">
                <form action="{{url('post/email/send')}}" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Имэйл илгээх</h4>
                    </div>
                    <div class="modal-body">
                        <p>Санал болгох найзынхаа имэйл хаягийг оруулна уу.</p>
                        {!! csrf_field() !!}
                        <input type="text" name="emailshare" class="form-control" placeholder="Имэйл хаяг оруулах">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Илгээх</button>
                    </div>
                </div>
                </form>
            </div>

    </div>

@stop