@extends('frontend.app')
@section('content')


    @include('frontend.partials.navbar')
    <div class="container">
        <h4 class="title" style="margin-top: 40px;color: #0a6aa1;text-transform: uppercase">
            Худалдан авалтын урамшуулал
        </h4>
        <h4 class="title" style="font-size: 16px;margin-top: 40px;">
            Таны худалдан авалт бүрийг урамшуулах үүднээс компанийн зүгээс дараах сугалаат урамшуулалыг санал болгож байна.
        </h4>
        <div class="block3 block-hotdeals">
            <div class="block-head">
                <p class="block-title">7-оос дээш удаа худалдаж авсан бол</p>
            </div>
            <div class="block-inner">
                <ul class="products kt-owl-carousel" data-margin="21" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"768":{"items":2},"1000":{"items":3},"1200":{"items":5}}'>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="block3 block-hotdeals">
            <div class="block-head">
                <p class="block-title">4-6 удаа худалдаж авсан бол</p>
            </div>
            <div class="block-inner">
                <ul class="products kt-owl-carousel" data-margin="21" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"768":{"items":2},"1000":{"items":3},"1200":{"items":5}}'>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>

                            </div>

                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="block3 block-hotdeals">
            <div class="block-head">
                <p class="block-title">1-3 удаа худалдаж авсан бол</p>
            </div>
            <div class="block-inner">
                <ul class="products kt-owl-carousel" data-margin="21" data-loop="true" data-nav="true" data-responsive='{"0":{"items":1},"600":{"items":3},"768":{"items":2},"1000":{"items":3},"1200":{"items":5}}'>
                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="product">
                        <div class="product-container">
                            <div class="product-left">
                                <div class="product-thumb">
                                    <a class="product-img" href="#"><img src="data/option1/p1.jpg" alt="Product"></a>
                                    <a title="Quick View" href="#" class="btn-quick-view">Quick View</a>
                                </div>
                            </div>
                            <div class="product-right">
                                <div class="product-name">
                                    <a href="#">Cotton Lycra Leggings</a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('frontend.partials.footer')
@stop