<header id="header">

    <!-- main menu-->
    <div class="main-menu">
        <div class="container">
            @if(Session::has('email-share'))
                <div class="row">
                    <div class="alert alert-success">
                        <span style="color: #ff0000;">{{Session::get('email-share')}}</span>
                    </div>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="logo" style="margin-top:7px;">
                        <a href="{{url('/')}}">
                            <img style="width: 50%;margin-left: 25%;margin-top: -25px;"
                                 src="{{asset('data/option1/khadag.png')}}" alt="Logo">
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 main-header-banner">
                    <nav class="navbar" id="main-menu">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <a class="navbar-brand" href="#">Цэс</a>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav" style="padding-left:25px;">

                                    <li class="dropdown">
                                        <a style="color: #175ca9;font-weight: bold;" href="/"
                                           class="dropdown-toggle" data-toggle="dropdown">ХАДАГ.КОМ</a>
                                        <ul class="dropdown-menu mega_dropdown container-fluid">
                                            <li class="block-container">
                                                <ul class="block-megamenu-link">
                                                    <li class="link_container">
                                                        <a href="/about">Бидний тухай</a>
                                                        <a href="/working">Хамтран ажиллах хялбар алхамууд</a>
                                                        <a href="/members">Гишүүдийн амжилтын түүхүүд</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="{{url('products-grid')}}" class="dropdown-toggle"
                                           data-toggle="dropdown">Бүтээгдэхүүнүүд</a>
                                        <ul class="dropdown-menu mega_dropdown container-fluid">
                                            <li class="block-container">
                                                <ul class="block-megamenu-link">
                                                    <li class="link_container">
                                                        @foreach($producttypes as $p)
                                                            <a href="{{url('products-cat',$p->id)}}">{!! $p->name !!}</a>
                                                        @endforeach
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>

                                    <li><a href="/newsshow">Мэдээлэл</a></li>
                                    <li><a href="/contact">Холбоо барих</a></li>
                                    @guest
                                        <li><a style="border: 2px solid #01953f;" href="/loginpage">Нэвтрэх</a></li>
                                        <li><a style="border: 2px solid #01953f;" href="/regiserpage">Бүртгүүлэх</a>
                                        </li>
                                    @else
                                        <li><a href="/user/cart">Миний сагс</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                               aria-expanded="false" aria-haspopup="true">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{url('user/account')}}">
                                                        Профайл
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="{{ route('logout') }}"
                                                       onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                        Гарах
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                          style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @endguest
                                </ul>
                            </div><!--/.nav-collapse -->

                        </div>
                    </nav>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <h4 style="font-style:italic;font-weight:600;margin-top:15px;color:#01953f;padding-left: 15px;">Мэд,
                    Мэдээл, Ашиг ол</h4>
            </div>
            <div class="col-sm-6 col-md-7">
                <div class="advanced-search box-radius">
                    <form class="form-inline" action="{{route('queries.search')}}" method="POST">

                        <div class="form-group search-category">
                            <select id="category-select" class="search-category-select">
                                <option value="1">Бүгд</option>
                                <option value="2">Нөхөн сэргээх</option>
                                <option value="3">Дархлааны</option>
                                <option value="3">Бие эрхтэн цэвэрлэх</option>
                            </select>
                        </div>
                        <div class="form-group search-input">
                            {!! csrf_field() !!}
                            <input type="text" name="search" placeholder="нэрээр хайх" style="width: 30vw;">
                        </div>
                        <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>

            <div class="col-sm-4 col-md-2 wrap-block-cl">
                <div class="inner-cl box-radius" style="border: none;">
                    <div class="block-social">
                        <ul class="list-social">
                            <li><a target="_blank" href="https://www.facebook.com/Luck-Life-Plus-409270776193436/"><i
                                            class="fab fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ./main menu-->
</header>