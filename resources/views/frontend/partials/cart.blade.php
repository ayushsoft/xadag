@extends('frontend.app')
@section('content')


    @include('frontend.partials.navbar')
    <div class="container">
        <div class="row">
            <div class="block block-breadcrumbs">
                <ul>
                    <li class="home">
                        <a href="#"><i class="fa fa-home"></i></a>
                        <span></span>
                    </li>
                    <li>Сагс</li>
                </ul>
            </div>
            <div class="main-page">
                <h3 class="page-title">Хэрэглэгчийн сагс</h3>
                <div class="page-content page-order">
                    <ul class="step">
                        <li class="current-step"></li>

                    </ul>
                    <div class="heading-counter warning">Таны сагсанд нийт:
                        <span style="color: green;">{{ $count }}</span>
                        бүтээгдэхүүн байна
                    </div>
                    <div class="order-detail-content">
                        <table class="cart_summary">
                            <thead>
                            <tr>
                                <th class="cart_product">Нэр</th>
                                <th>Тухай</th>
                                <th>Үдсэн үнэ</th>
                                <th>Хямдарсан үнэ</th>
                                <th>Тоо</th>
                                <th>Нийт</th>
                                <th class="action"><i class="fa fa-trash-o"></i></th>
                            </tr>
                            </thead>
                            @foreach($cart_products as $item)
                            <tbody>
                                <tr>
                                    <td class="cart_product">
                                        <a href="#"><img class="img-responsive" src="/uploads/product/{{$item->products->image}}" alt="Product"></a>
                                    </td>
                                    <td class="cart_description">
                                        <p class="product-name">{{$item->products->name}}</p>
                                        <small class="cart_ref">Хадгалах хугацаа: {{$item->products->save_time}}</small>
                                        <br>
                                        <small><a href="#">Color : Beige</a></small>
                                        <br>
                                        <small><a href="#">Size : S</a></small>
                                    </td>
                                    <td class="price"><span>{{$item->products->price}}₮</span></td>
                                    <td class="price"><span>{{$item->products->price}}₮</span></td>
                                    <td class="qty">
                                        <input class="form-control input-sm" type="text" value="1">


                                    </td>
                                    <td class="price">
                                        <span>{{$item->total}}₮</span>
                                    </td>
                                    <td class="action">
                                        <a href="#">Delete item</a>
                                    </td>
                                </tr>

                            </tbody>
                            @endforeach
                            <tfoot>
                            <tr>
                                <td colspan="3"><strong>Нийт төлөх:</strong></td>
                                <td colspan="2"><strong>{{$cart_total}}₮</strong></td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="cart_navigation">
                            <a class="button" href="#"><i class="fa fa-angle-left"></i> Буцах</a>
                            <a class="button pull-right" href="{{url('cart/checkout')}}">Үргэлжлүүлэх <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('frontend.partials.footer')
@stop