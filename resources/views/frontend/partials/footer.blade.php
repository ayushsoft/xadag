<footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="block footer-block-box">
                                <div class="block-head">
                                    <div class="block-title">
                                        <div class="block-icon">
                                            <img src="{{asset('data/location-icon.png')}}" alt="store icon">
                                        </div>
                                        <div class="block-title-text text-sm">Найздаа 10%-ийн хөнгөлөлтийг санал болгоод</div>
                                        <div class="block-title-text text-lg" 
                                            style="margin-left: 18%;line-height: 30px;color: #01953f;">Орлого ол!</div>
                                    </div>
                                </div>
                                <div class="block-inner">
                                    <div class="block-info clearfix">
                                      
                                    </div>
                                    <div class="block-input-box box-radius clearfix" style="border: none;">
                                        
                                        <a href="/regiserpage" class="block-button" style="cursor: pointer;float: left;margin-right: 65%;">Бүртгүүлэх</a>
                                        <a href="/loginpage" style="cursor: pointer;" class="block-button">Нэвтрэх</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="block footer-block-box">
                                <div class="block-head">
                                    <div class="block-title">
                                        <div class="block-icon">
                                            <img src="{{asset('data/email-icon.png')}}" alt="store icon">
                                        </div>
                                        <div class="block-title-text text-sm">Шинэ мэдээ мэдээллийг цаг алдалгүй</div>
                                        <div class="block-title-text text-lg" style="margin-left: 18%;line-height: 30px;color: #01953f;"> ИМЭЙЛ хаягаар авах</div>
                                    </div>
                                </div>
                                <div class="block-inner">
                                    <div class="block-info clearfix">
                                       
                                    </div>
                                    <form action="{{url('post/email/send')}}" method="POST">
                                        <div class="block-input-box box-radius clearfix">
                                            {!! csrf_field() !!}
                                            <input type="text" name="email" class="input-box-text" placeholder="ИМЭЙЛ ХАЯГ">
                                            <button type="submit" class="block-button">Илгээх</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="block footer-block-box">
                                <div class="block-head">
                                    <div class="block-title">
                                        <div class="block-icon">
                                            <img src="data/partners-icon.png" alt="store icon">
                                        </div>
                                        <div class="block-title-text text-sm">Апп</div>
                                        <div class="block-title-text text-lg">Татах</div>
                                    </div>
                                </div>
                                <div class="block-inner">
                                    <div class="block-owl">
                                        <ul class="kt-owl-carousel list-partners" data-nav="true" data-autoplay="true" data-loop="true" data-items="1">
                                            <li class="partner"><a href="#"><img src="{{asset('data/apple.png')}}" alt="partner"></a></li>
                                            <li class="partner"><a href="#"><img src="{{asset('data/android.png')}}" alt="partner"></a></li>
                                                
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 block-link-wapper">
                            <div class="block-link">
                                <ul class="list-link">
                                    <li class="head" style="color: #175ca9;font-weight: bold"><a href="/">ХАДАГ.КОМ</a></li>
                                    <li><a href="/about">Бидний тухай</a></li>
                                    <li><a href="/working">Хамтран ажиллах хялбар алхамууд</a></li>
                                    <li><a href="/bonus">Урамшуулал</a></li>
                                    <li><a href="/members">Гишүүдийн амжилтын түүхүүд</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 block-link-wapper">
                            <div class="block-link">
                                <ul class="list-link">
                                    <li class="head"><a href="javascript:void(0)">Бүтээгдэхүүн</a></li>
                                     @foreach($producttypes as $p)
                                    <li>
                                        <a href="{{url('products-cat',$p->id)}}">{!! $p->name !!}</a>
                                    </li>
                                    @endforeach
                                    
                                 </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 block-link-wapper">
                            <div class="block-link">
                                <ul class="list-link">
                                    <li class="head">Холбоо барих</li>
                                    <li>Хаяг: 44-33, БГД, Улаанбаатар</li>
                                    <li>Утас: ( +976 ) 99030783</li>
                                    <li>И-мэйл: info@lucklife.mn</li>
                                 </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                   <div class="col-md-4"></div>
                   <div class="col-md-4">
                       <div class="block-coppyright">
                           <p style="margin-left: 50%;">
                               © 2018 Xadag.com
                           </p>
                       </div>
                   </div>
                   <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </footer>