<!doctype html>
<html lang="{{ app()->getLocale() }}">
        <head>
        <meta charset="utf-8">
       	<meta property="og:url" content="http://www.xadag.com/products/@yield('og-url')">
        <meta property="og:type" content="website">
        <meta property="og:title" content="@yield('og-title')">
        <meta property="og:site_name" content="@yield('site-title')"/>
        <meta property="og:description" content="@yield('og-description')">
        <meta property="og:image" content="http://xadag.com/uploads/product/@yield('og-image')">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
       
        <title>Xadag.com</title>
       
        <!-- Fonts -->

        <!-- Styles -->
         <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/reset.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/lib/bootstrap/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/lib/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/lib/owl.carousel/owl.carousel.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/lib/jquery-ui/jquery-ui.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/animate.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/global.css')}}" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/responsive.css')}}" />
     <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/swiper.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('front/assets/css/option2.css')}}" />
  
    </head>
    <body class="option2">

        @yield('content')
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/jquery/jquery-1.11.2.min.js')}}"></script>
   <!--  <script
      src="http://code.jquery.com/jquery-2.2.4.js"
      integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
      crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/jquery.bxslider/jquery.bxslider.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/owl.carousel/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- COUNTDOWN -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/countdown/jquery.plugin.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/lib/countdown/jquery.countdown.js')}}"></script>
    <!-- ./COUNTDOWN -->
        <script type="text/javascript" src="{{URL::asset('front/assets/js/demo.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/js/jquery.actual.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('front/assets/js/jquery.printPage.js')}}"></script> 
    <script type="text/javascript" src="{{URL::asset('front/assets/js/swiper.min.js')}}"></script> 
    <script type="text/javascript" src="{{URL::asset('front/assets/js/script.js')}}"></script>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=237722380007266&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    
    <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=304265309959276";
      fjs.parentNode.insertBefore(js, fjs);
    }
    (document, 'script', 'facebook-jssdk'));
    
    window.twttr = (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0],
        t = window.twttr || {};
      if (d.getElementById(id)) return t;
      js = d.createElement(s);
      js.id = id;
      js.src = "https://platform.twitter.com/widgets.js";
      fjs.parentNode.insertBefore(js, fjs);
    
      t._e = [];
      t.ready = function(f) {
        t._e.push(f);
      };
    
      return t;
    }(document, "script", "twitter-wjs"));
    </script>
    <script>
      function printDiv(print) {

         var printContents = document.getElementById("print").innerHTML;
         var originalContents = document.body.innerHTML;

         document.body.innerHTML = printContents;

         window.print();

         document.body.innerHTML = originalContents;

      }
    </script>
    <script>
            var swiper = new Swiper('.swiper-container', {
                spaceBetween: 30,
                centeredSlides: true,
                autoplay: {
                    delay: 3500,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        </script>
    </body>
</html>
