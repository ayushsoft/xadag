<?php

use App\Product_type;
use App\Product;
use App\ProductImg;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@frontNews');



Route::get('/newsshow','NewsController@pageshowall');
Route::get('/products/{id}/{code?}','HomeController@productDesc');



Route::get('/products-grid','HomeController@productGrid');
Route::get('/products-cat/{id}','HomeController@productCat');
Route::get('/bonus','HomeController@bonus');

//user profile

Route::get('/user/cart','UserController@index');
Route::post('/user/cart/add','UserController@cardAdd');
Route::get('/user/account/info','UserController@index');
Route::get('/user/account','UserController@show');
Route::get('/user/profile','UserController@profile');
Route::get('/cart/checkout','UserController@checkout');

//search

Route::post('/queries', [
    'uses' => '\App\Http\Controllers\HomeController@search',
    'as'   => 'queries.search',
]);

//
//email send
Route::get('/email/send/{id}','EmailController@send');
Route::post('/post/email/send','EmailController@post');
//

Route::get('/about', function () {
    $producttypes = Product_type::all();
     $product = Product::all();
 
    return view('frontend.pages.about',compact('producttypes','product'));
});

Route::get('/working', function () {
    $producttypes = Product_type::all();
    
    return view('frontend.pages.working',compact('producttypes'));
});
Route::get('/members', function () {
    $producttypes = Product_type::all();
    return view('frontend.pages.members',compact('producttypes'));
});
Route::get('/contact', function () {
    $producttypes = Product_type::all();
    return view('frontend.pages.contact',compact('producttypes'));
});


Route::get('/loginpage', function () {
    $producttypes = Product_type::all();
    return view('frontend.auth.login',compact('producttypes'));
});



Route::get('/regiserpage', function () {
    $producttypes = Product_type::all();
    return view('frontend.auth.register',compact('producttypes'));
});


Route::group(['middleware' => 'is-admin'], function () {     
    
    
});

Route::get('/admin', function () {
    return view('backend.home');
});

Route::group(['middleware' => ['auth']], function () {

		
	Route::get('/admin', function () {
				    return view('backend.home');
				});

				Route::resource('menu', 'MenuController');

				Route::resource('news', 'NewsController');
				Route::get('/deletenews/{id}','NewsController@destroy');

				Route::resource('product', 'ProductController');
				Route::get('/deleteproduct/{id}','ProductController@destroy');
				Route::get('/enableproduct/{id}','ProductController@enable');
				Route::get('/disableproduct/{id}','ProductController@disable');
				

				Route::resource('producttype', 'ProductTypeController');
				Route::get('/deleteproducttype/{id}','ProductTypeController@destroy');

				Route::resource('productimg','ProductImgController');
				Route::delete('/productimgdelete/{id}','ProductImgController@destroy');

                Route::resource('commend','CommendController');
                
				Route::resource('cart', 'CartController');

				Route::resource('incentive', 'IncentiveController');
					Route::get('/deletecommend/{id}','CommendController@destroy');
		Route::group(['middleware' => 'admin'], function () {
  		Route::get('/admin', function () {
				    return view('backend.home');
				});
  	

 });

    });

Route::post('/registeruser','HomeController@createUser');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Auth::routes();



Route::get('/home', 'HomeController@frontNews');
